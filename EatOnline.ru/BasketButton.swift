//
//  BasketButton.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 07.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class BasketButton: UIBarButtonItem {
    
    let badgeLabel = UILabel(frame: CGRect(x: 23.0, y: 3.0, width: 20.0, height: 20.0))
    
    convenience init(badgeNumber: Int, darkStyle: Bool = false) {
        let actionButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0))
        actionButton.setImage(UIImage(named: "nav_cart"), for: .normal)
        self.init(customView: actionButton)
        badgeLabel.backgroundColor = darkStyle ? .black : .appRed
        badgeLabel.layer.borderColor = UIColor.white.cgColor
        badgeLabel.layer.borderWidth = 1.5
        badgeLabel.layer.masksToBounds = true
        badgeLabel.layer.cornerRadius = 10.0
        badgeLabel.textAlignment = .center
        badgeLabel.font = .systemFont(ofSize: 10.0)
        badgeLabel.textColor = .white
        actionButton.addSubview(badgeLabel)
        actionButton.addTarget(self, action: #selector(showBasket), for: .touchDown)
        setBadgeNumber(badgeNumber, animated: false)
        NotificationCenter.subscribe(observer: self, selector: #selector(basketChanged), type: .basketChanged)
        NotificationCenter.subscribe(observer: self, selector: #selector(basketCleared), type: .basketCleared)
    }
        
    private override init() {
        super.init()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setBadgeNumber(_ count: Int, animated: Bool = true) {
        self.isEnabled = count > 0
        if count == 0 {
            UIView.animate(withDuration: animated ? 0.1 : 0) {
                self.badgeLabel.transform = CGAffineTransform(scaleX: 0, y: 0)
                self.badgeLabel.text = nil
            }
        } else {
            UIView.animate(withDuration: animated ? 0.1 : 0, animations: {
                self.badgeLabel.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.badgeLabel.text = "\(count)"
            }) { (finished) in
                self.badgeLabel.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
        }
    }
    
    @objc func basketChanged() {
        setBadgeNumber(Basket.current.currentOrder.items.count)
    }
    
    @objc func basketCleared() {
        setBadgeNumber(0)
    }
    
    @objc private func showBasket() {
        Router.shared.pushController(.basket)
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
