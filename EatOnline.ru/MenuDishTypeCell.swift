//
//  MenuDishTypeCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 21.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class MenuDishTypeCell: UniversalTableViewCell {

    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var iconWidth: NSLayoutConstraint!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var arrow: UIImageView!
    @IBOutlet var separatorView: UIView!
    @IBOutlet var separatorHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        separatorHeight.constant = 0.5
    }
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let content = content as? (DishType, Bool) else { return }
        if content.0.type == nil {
            arrow.image = UIImage(named: "cell_arrow_down")
            iconWidth.constant = 26
            indicator.startAnimating()
            icon.setImage(path: content.0.iconPath) {
                self.indicator.stopAnimating()
            }
            separatorView.isHidden = false
        } else {
            arrow.image = UIImage(named: "cell_arrow_down_small")
            indicator.stopAnimating()
            iconWidth.constant = 0
            separatorView.isHidden = true
        }
        nameLabel.text = content.0.name
        arrow.rotate(angle: content.1 ? CGFloat.pi : 0)
    }
        
}

extension Cell {
    convenience init(menuDishType: DishType, expanded: Bool) {
        self.init(cellType: .menuDishType)
        self.content = (menuDishType, expanded)
    }
}
