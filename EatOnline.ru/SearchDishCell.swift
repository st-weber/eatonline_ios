//
//  SearchDishCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 05/11/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class SearchDishCell: UniversalTableViewCell {

    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var dishImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var placeLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var favouriteButton: UIButton!
    
    weak var dish: Dish?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let content = content as? (Dish, String?) else { return }
        self.dish = content.0
        
        indicator.startAnimating()
        dishImage.setImage(path: content.0.imagePath) {
            self.indicator.stopAnimating()
        }
        
        nameLabel.text = content.0.name
        
        if let searchString = content.1, let dishName = content.0.name {
            let nsDishName = NSString(string: dishName.lowercased())
            let searchRange = nsDishName.range(of: searchString.lowercased())
            let attributedString = NSMutableAttributedString(string:dishName)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.appRed, range: searchRange)
            nameLabel.attributedText = attributedString
        }
        
        placeLabel.text = content.0.place?.name
        weightLabel.text = content.0.getWeightText()
        priceLabel.text = content.0.getPrice()
        updateFavouriteButton()
    }
    
    @IBAction func favouritePressed(_ sender: UIButton) {
        dish?.switchFavourite()
        updateFavouriteButton()
    }
    
    func updateFavouriteButton() {
        guard let dish = dish else { return }
        favouriteButton.setImage(UIImage(named: dish.favourite ? "favourite_active" : "favourite_passive"), for: .normal)
    }
    
}

extension Cell {
    convenience init(searchDish: Dish, searchText: String? = nil) {
        self.init(cellType: .searchDish)
        self.content = (searchDish, searchText)
    }
}
