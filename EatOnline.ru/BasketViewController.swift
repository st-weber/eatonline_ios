//
//  BasketViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class BasketViewController: UniversalViewController {

    @IBOutlet var itemsTable: TableView!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var minimalLabel: UILabel!
    @IBOutlet var arrowImage: UIImageView!
    @IBOutlet var checkoutButton: UIButton!
    @IBOutlet var bottomLineHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        self.customTableView = itemsTable
        self.showBasket = false
        self.DS = CurrentBasketDS(tableInteractor: self)
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_clear_basket"), style: .plain, target: self, action: #selector(clearBasket))
        updateTable()
        
        guard let place = Basket.current.currentOrder.place else { return }
        minimalLabel.text = "мин. заказ: \(place.minimalOrder)р."
        checkSummary()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.subscribe(observer: self, selector: #selector(checkSummary), type: .basketChanged)
        NotificationCenter.subscribe(observer: self, selector: #selector(goBack), type: .basketCleared)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.unsubscribe(self)
    }
    
    @objc func checkSummary() {
        guard let place = Basket.current.currentOrder.place else { return }
        let summary = Basket.current.currentOrder.price
        summaryLabel.text = "Заказ на сумму: \(summary)р."
        
        if summary <= Int(place.minimalOrder) {
            arrowImage.isHidden = true
            checkoutButton.isHidden = true
            minimalLabel.isHidden = false
            if bottomLineHeight.constant > 20 {
                bottomLineHeight.constant = 20.0
                UIView.animate(withDuration: 0.1) {
                    self.view.layoutIfNeeded()
                }
            }
        } else {
            arrowImage.isHidden = false
            checkoutButton.isHidden = false
            minimalLabel.isHidden = true
            if bottomLineHeight.constant < 44 {
                bottomLineHeight.constant = 44.0
                UIView.animate(withDuration: 0.1) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func clearBasket() {
        Basket.current.clear()
    }
    
    @IBAction func checkoutPressed(_ sender: Any) {
        if let closedText = Basket.current.currentOrder.place?.isClosed {
            Alert.show(title: "Заведение закрыто", message: closedText, actions: nil)
            return
        }
        Router.shared.pushController(.checkout)
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
