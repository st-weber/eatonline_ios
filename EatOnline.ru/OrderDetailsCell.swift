//
//  OrderDetailsCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 20.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class OrderDetailsCell: UniversalTableViewCell {

    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var addressCaption: UILabel!
    @IBOutlet var addressCaptionTop: NSLayoutConstraint!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var forTimeCaption: UILabel!
    @IBOutlet var forTimeTop: NSLayoutConstraint!
    @IBOutlet var forTimeLabel: UILabel!
    @IBOutlet var personsLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let order = content as? Order else { return }
        
        statusLabel.text = order.statusTitle
        typeLabel.text = order.type == 0 ? "доставка" : "на вынос"
        
        if let address = order.address, address.count > 0 {
            addressCaption.text = "Адрес"
            addressCaptionTop.constant = 20
            addressLabel.text = address
        } else {
            addressCaption.text = nil
            addressCaptionTop.constant = 0
            addressLabel.text = nil
        }
        
        if let forTime = order.forTime, forTime.count > 0 {
            forTimeCaption.text = "К этому времени"
            forTimeTop.constant = 20
            forTimeLabel.text = forTime
        } else {
            forTimeCaption.text = nil
            forTimeTop.constant = 0
            forTimeLabel.text = nil
        }

        personsLabel.text = "\(order.persons)"
        priceLabel.text = "\(order.finalPrice) р."
    }
    
}

extension Cell {
    convenience init(orderDetails: Order) {
        self.init(cellType: .orderDetails)
        self.content = orderDetails
    }
}
