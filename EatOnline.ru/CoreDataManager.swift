//
//  CoreDataManager.swift
//  MyMot
//
//  Created by Michail Solyanic on 04/04/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager {

    static let instance = CoreDataManager()
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "EATonline")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        container.viewContext.undoManager = nil
        container.viewContext.shouldDeleteInaccessibleFaults = true
        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func saveOtherContext(_ context: NSManagedObjectContext, saved: (()->())?) {
        // https://medium.com/swift2go/fetching-remote-data-with-core-data-background-context-in-ios-app-224dad15ef6c
        context.perform {
          self.importDefaultData(childContext: context) // set up the database for the new game
          do {
            try context.save()
            self.persistentContainer.viewContext.performAndWait {
                do {
                    self.persistentContainer.viewContext.mergeChanges(fromContextDidSave: Notification(name: .NSManagedObjectContextDidSave, object: context, userInfo: nil))
                    try self.persistentContainer.viewContext.save()
                    saved?()
                } catch {
                    fatalError("Failure to save context: \(error)")
                }
            }
          } catch {
            fatalError("Failure to save context: \(error)")
          }
        }
        //context.reset()
    }
    
    func importDefaultData(childContext: NSManagedObjectContext) {
        saveChildContext(childContext: childContext)
    }

    func saveChildContext(childContext: NSManagedObjectContext) {
        guard childContext.hasChanges else {
            return
        }
        do {
            try childContext.save()
        } catch {
            let nserror = error as NSError
            print(nserror.localizedDescription)
        }
    }
    
}
