//
//  MSAction+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSAction+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSAction (CoreDataProperties)

+ (NSFetchRequest<MSAction *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *identifier;
@property (nonatomic) int16_t imageHeight;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nonatomic) int16_t imageWidth;
@property (nonatomic) BOOL onMainScreen;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) int16_t type;
@property (nullable, nonatomic, retain) MSPlace *place;

@end

NS_ASSUME_NONNULL_END
