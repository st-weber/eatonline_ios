//
//  AppDelegate.swift
//  MyMot
//
//  Created by Michail Solyanic on 02/04/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit
import Firebase
import AVKit
import AVFoundation
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var restrictRotation: UIInterfaceOrientationMask = .portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        PushNotificationCenter.shared.delegate = self
        UNUserNotificationCenter.current().delegate = self
        PushNotificationCenter.shared.dispatchAtAppStartUp(launchOptions)
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = .black
        Router.shared.startApp()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.instance.saveContext()
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        switch shortcutItem.type {
        case "hotline":
            ConfigStorage.shared.hotlinePhone?.call()
            /*let number = ConfigStorage.shared.hotlinePhone?.call()
            if let url = NSURL(string: "tel://\(String(describing: number))"), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }*/
        case "wrote_us":
            Router.shared.pushController(.wroteUs)
            
        default:
            break
        }
        
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        PushNotificationCenter.shared.dispatchWhenAppRunning(response.notification.request.content.userInfo)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        PushNotificationCenter.shared.showWhenAppRunning(notification.request.content.userInfo)
        // TODO: need to replace into showWhenAppRunning
        NotificationCenter.post(type: .onNotificationReceived)
        completionHandler([.alert])
    }
}

extension AppDelegate: PushNotificationCenterDelegate {
    
    func didReceive(firebase token: String) {
        print("didReceive token: \(token)")
        if ConfigStorage.shared.firebaseToken == nil || ConfigStorage.shared.firebaseToken != token {
            if ConfigStorage.shared.firebaseToken == nil {
                ConfigStorage.shared.pushesEnabled = true
            }
            ConfigStorage.shared.firebaseToken = token
            let interactor = ApiInteractor()
            interactor.pushSubscribe { (error) in
                if let error = error {
                    Alert.showError(error)
                }
            }
        }
    }
    
    func didReceive(firebase push: PushNotification, isRuntime: Bool) {
        switch push.eventType {
        /*case .newPlaceMessage, .placeAdvertMessage:
            guard let placeId = push.serviceContent as? Int,
                let place = CoreDataManager.instance.getPlaceById(placeId) else { return }
            Router.shared.pushController(.place(place, nil, nil))*/
        default:
            break
        }
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let dataDict: [String: String] = ["token": fcmToken]
        NotificationCenter.post(type: .fcmToken, object: nil, userInfo: dataDict)
    }
    
}
