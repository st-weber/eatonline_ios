//
//  CellTypes.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

enum CellType {
    
    case simple
    case property
    case switcher
    case billCell
    case placeList
    case placeInfo
    case dishType
    case searchDish
    case menuDish
    case menuDishType
    case basketCell
    case order
    case orderDetails
    case orderItem
    
    var cellClass: Any {
        switch self {
        case .simple:
            return SimpleCell.self
        case .property:
            return PropertyCell.self
        case .switcher:
            return SwitchCell.self
        case .billCell:
            return BillCell.self
        case .dishType:
            return DishTypeCell.self
        case .placeList:
            return PlaceListCell.self
        case .placeInfo:
            return PlaceInfoCell.self
        case .searchDish:
            return SearchDishCell.self
        case .menuDish:
            return MenuDishCell.self
        case .menuDishType:
            return MenuDishTypeCell.self
        case .basketCell:
            return BasketCell.self
        case .order:
            return OrderCell.self
        case .orderDetails:
            return OrderDetailsCell.self
        case .orderItem:
            return OrderItemCell.self
        }
    }
    
    var height: CGFloat {
        switch self {
        case .dishType:
            return 70.0
        default:
            return UITableView.automaticDimension
        }
    }
    
    var width: CGFloat {
        switch self {
        default:
            return CGFloat.leastNonzeroMagnitude
        }
    }
}
