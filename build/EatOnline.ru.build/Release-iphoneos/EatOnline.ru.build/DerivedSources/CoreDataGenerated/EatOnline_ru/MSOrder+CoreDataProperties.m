//
//  MSOrder+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSOrder+CoreDataProperties.h"

@implementation MSOrder (CoreDataProperties)

+ (NSFetchRequest<MSOrder *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSOrder"];
}

@dynamic date;
@dynamic finalPrice;
@dynamic number;
@dynamic status;
@dynamic type;
@dynamic amounts;
@dynamic place;
@dynamic table;

@end
