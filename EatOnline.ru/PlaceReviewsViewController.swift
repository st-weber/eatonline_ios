//
//  PlaceReviewsViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 18.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class PlaceReviewsViewController: UniversalViewController {

    let placeId: Int
    
    @IBOutlet var reviewsTable: TableView!
    
    init(placeId: Int) {
        self.placeId = placeId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reviewsTable.isHidden = true
        
    }

}
