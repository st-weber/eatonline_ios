//
//  UIFont.swift
//  MyMot
//
//  Created by Michail Solyanic on 03/04/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func duke(size: CGFloat) -> UIFont {
        return UIFont(name: "AA Duke", size: size)!
    }
    
}
