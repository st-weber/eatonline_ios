//
//  AddressSuggestion.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 23.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import SwiftyJSON

struct AddressSuggestion: ResponseSerializable {

    let value: String
    var geoLat: String?
    var geoLon: String?
    var streetAndHouse: String?
    
    
    init?(json: JSON) {
        guard let value = json.dictionary?["value"]?.string else { return nil }
        self.value = value
        self.geoLat = json.dictionary?["data"]?.dictionary?["geo_lat"]?.string
        self.geoLon = json.dictionary?["data"]?.dictionary?["geo_lon"]?.string
        
        if let street = json.dictionary?["data"]?.dictionary?["street_with_type"]?.string,
            let houseType = json.dictionary?["data"]?.dictionary?["house_type"]?.string,
            let house = json.dictionary?["data"]?.dictionary?["house"]?.string {
            streetAndHouse = "\(street), \(houseType) \(house)"
        }
    }
    
}
