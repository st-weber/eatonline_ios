//
//  DishesDS.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 15.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class FavouriteDishesDS: DataSource {

    var dishesMap: [Int32:Int] = [:]
    var becomeEmpty: ((Bool)->())?
    
    override init(tableInteractor: DataUpdateProtocol) {
        super.init(tableInteractor: tableInteractor)
        NotificationCenter.subscribe(observer: self, selector: #selector(dishUpdated), type: .favouriteDishSwitched)
    }
    
    override func prepareData() {
        let section = Section()
        dishesMap = [:]
        
        let dishesList = CoreDataManager.instance.getFavouriteDishes()
        for (index, dish) in dishesList.enumerated() {
            let dishCell = Cell(searchDish: dish)
            dishCell.cellTapped = { indexPath in
                if let place = dish.place {
                    Router.shared.pushController(.place(place, nil, dish))
                }
            }
            section.cells.append(dishCell)
            dishesMap[dish.id] = index
        }
        
        dataSource = [section]
        becomeEmpty?(dataSource[0].cells.count == 0)
    }
    
    @objc func dishUpdated(notification: Notification) {
        guard let dish = notification.object as? Dish else { return }
        if !dish.favourite, let index = dishesMap[dish.id] {
            let indexPath = IndexPath(row: index, section: 0)
            dataSource[0].cells.remove(at: index)
            tableInteractor?.deleteRows(indexPaths: [indexPath], animation: .automatic)
            dishesMap.removeValue(forKey: dish.id)
            for dishPos in dishesMap {
                if dishPos.value > index {
                    dishesMap[dishPos.key] = dishPos.value - 1
                }
            }
        } else {
            prepareData()
            tableInteractor?.updateTable()
        }
        if dataSource[0].cells.count == 1 {
            becomeEmpty?(false)
        } else if dataSource[0].cells.count == 0 {
            becomeEmpty?(true)
        }
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
