//
//  MSPlace+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSPlace+CoreDataProperties.h"

@implementation MSPlace (CoreDataProperties)

+ (NSFetchRequest<MSPlace *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSPlace"];
}

@dynamic address;
@dynamic backgroundPath;
@dynamic closeHour;
@dynamic coordinates;
@dynamic deliveryFrom;
@dynamic deliveryPrice;
@dynamic favourite;
@dynamic footNote;
@dynamic identifier;
@dynamic kitchens;
@dynamic lastUpdate;
@dynamic logoPath;
@dynamic minimalOrder;
@dynamic onlinePayment;
@dynamic openHour;
@dynamic output;
@dynamic serviceDelivery;
@dynamic serviceReservation;
@dynamic serviceTakeaway;
@dynamic sort;
@dynamic title;
@dynamic type;
@dynamic updated;
@dynamic workTime;
@dynamic action;
@dynamic city;
@dynamic dishes;
@dynamic dishTypes;
@dynamic orders;
@dynamic tables;

@end
