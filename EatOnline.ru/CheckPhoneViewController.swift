//
//  CheckPhoneViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 17.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit
import InputMask

class CheckPhoneViewController: UniversalViewController {

    let phone: String
    let authorisationMode: Bool
    var maskDelegate = MaskedTextFieldDelegate(primaryFormat: String.codeMask)
    
    let apiInteractor = ApiInteractor()
    var resendTimer = Timer()
    
    @IBOutlet var codeField: UITextField!
    @IBOutlet var hintLabel: UILabel!
    @IBOutlet var resendButton: UIButton!
    @IBOutlet var correctImage: UIImageView!
    @IBOutlet var checkIndicator: UIActivityIndicatorView!
    
    init(phone: String, authorisationMode: Bool) {
        self.phone = phone
        self.authorisationMode = authorisationMode
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.showBasket = false
        super.viewDidLoad()
        hintLabel.text = "Пожалуйста, дождитесь СМС с проверочным \nкодом на номер: \(phone)"
        maskDelegate.listener = self
        codeField.delegate = maskDelegate
        requestCode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        codeField.becomeFirstResponder()
    }
    
    func requestCode() {
        showProgress()
        apiInteractor.requestCode(phone: phone) { (delay, current, error) in
            self.hideProgress()
            if let error = error {
                Alert.showError(error) {
                    self.requestCode()
                }
            } else if let delay = delay, let current = current {
                self.startResendTimer(delay: delay - current)
            }
        }
    }
    
    func startResendTimer(delay: Int) {
        resendTimer.invalidate()
        resendTimer = Timer.scheduledTimer(timeInterval: TimeInterval(delay), target: self, selector: #selector(enableResend), userInfo: nil, repeats: false)
    }
    
    @objc func enableResend() {
        resendButton.isEnabled = true
    }
    
    @IBAction func resendPressed(_ sender: Any) {
        resendButton.isEnabled = false
        requestCode()
    }
    
    func checkCode(_ code: String) {
        showProgress()
        apiInteractor.checkCode(phone: phone, code: code) { (valid) in
            if valid {
                if !self.authorisationMode {
                    self.apiInteractor.createOrder { (orderNumber) in
                        self.hideProgress()
                        if let orderNumber = orderNumber {
                            Basket.current.clear()
                            NotificationCenter.post(type: .basketChanged)
                            Router.shared.pushController(.orderCreated(orderNumber))
                        }
                    }
                } else {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            } else {
                self.hideProgress()
                Alert.showErrorMessage("не верный проверочный код")
            }
        }
    }
    
}

extension CheckPhoneViewController: MaskedTextFieldDelegateListener {
    
    @objc func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if let code = textField.text, code.count == 4 {
            self.view.endEditing(true)
            self.checkCode(code)
        }
    }
    
}
