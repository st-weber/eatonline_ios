//
//  Action.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 29.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

extension Action {
    
    var markerImage: UIImage? {
        switch type {
        case 33:
            return UIImage(named: "bill_action")
        case 34:
            return UIImage(named: "bill_sale")
        case 62:
            return UIImage(named: "bill_news")
        default:
            return nil
        }
    }
    
    
}
