//
//  MSOrderAmount+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSOrderAmount+CoreDataProperties.h"

@implementation MSOrderAmount (CoreDataProperties)

+ (NSFetchRequest<MSOrderAmount *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSOrderAmount"];
}

@dynamic amount;
@dynamic dish;
@dynamic order;

@end
