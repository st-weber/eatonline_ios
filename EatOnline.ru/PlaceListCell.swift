//
//  PlaceListCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class PlaceListCell: UniversalTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var kitchensLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var fadeView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var closedLabel: UILabel!
    @IBOutlet weak var actionImage: UIImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    
    weak var place: Place?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let content = content as? (Place, String?) else { return }
        let place = content.0
        self.place = place
        
        titleLabel.text = place.name//! + " - \(place.sort)"
        
        if let searchString = content.1, let placeName = place.name {
            let nsPlaceName = NSString(string: placeName.lowercased())
            let searchRange = nsPlaceName.range(of: searchString.lowercased())
            let attributedString = NSMutableAttributedString(string:placeName)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.appRed, range: searchRange)
            titleLabel.attributedText = attributedString
        }
        
        timeLabel.text = place.workTime
        kitchensLabel.text = place.kitchens
        paymentLabel.text = place.paymentText
        deliveryLabel.text = place.deliveryText
        indicator.startAnimating()
        logoImage.setImage(path: place.logoPath, completed: {
            self.indicator.stopAnimating()
        })
        
        if let actionMarker = place.action?.markerImage {
            actionImage.image = actionMarker
            actionImage.isHidden = false
        } else {
            actionImage.isHidden = true
        }
        
        if let unavailableText = place.isUnavailable {
            fadeView.isHidden = false
            closedLabel.text = unavailableText
            logoImage.alpha = 0.3
        } else if let closedText = place.isClosed {
            fadeView.isHidden = false
            closedLabel.text = closedText
            logoImage.alpha = 0.3
        } else {
            fadeView.isHidden = true
            closedLabel.text = nil
            logoImage.alpha = 1
        }
        
        updateFavouriteButton()
    }
    
    @IBAction func favouritePressed(_ sender: UIButton) {
        place?.switchFavourite()
        updateFavouriteButton()
    }
    
    func updateFavouriteButton() {
        guard let place = place else { return }
        favouriteButton.setImage(UIImage(named: place.favourite ? "favourite_active" : "favourite_passive"), for: .normal)
    }
    
}

extension Cell {
    convenience init(placeList: Place, searchText: String? = nil) {
        self.init(cellType: .placeList)
        self.content = (placeList, searchText)
    }
}
