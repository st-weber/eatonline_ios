//
//  SearchDS.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 21.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class SearchDS: DataSource {

    override func prepareData() {
        dataSource = [Section()]
    }
    
    func searchUpdated(_ searchText: String) {
        if searchText.count > 1 {
            var sections: [Section] = []
            let places = CoreDataManager.instance.searchPlacesByName(searchText)
            if places.count > 0 {
                let placesSection = Section()
                placesSection.headerProperties.title = "Заведения"
                for place in places {
                    
                    let placeCell = Cell(placeList: place, searchText: searchText)
                    placeCell.cellTapped = { indexPath in
                        if !place.needToUpdate {
                            Router.shared.pushController(.place(place, nil, nil))
                        }
                    }
                    placesSection.cells.append(placeCell)
                    
                }
                sections.append(placesSection)
            }
            
            let dishes = CoreDataManager.instance.searchDishesByName(searchText)
            if dishes.count > 0 {
                let dishesSection = Section()
                dishesSection.headerProperties.title = "Блюда"
                for dish in dishes {
                    let dishCell = Cell(searchDish: dish, searchText: searchText)
                    dishCell.cellTapped = { indexPath in
                        if let place = dish.place {
                            Router.shared.pushController(.place(place, nil, dish))
                        }
                    }
                    dishesSection.cells.append(dishCell)
                }
                sections.append(dishesSection)
            }
            
            if sections.count > 0 {
                dataSource = sections
            } else {
                dataSource = [Section()]
            }
        } else {
            dataSource = [Section()]
        }
        tableInteractor?.updateTable()
    }
    
}
