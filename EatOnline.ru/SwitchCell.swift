//
//  SwitchCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class SwitchCell: UniversalTableViewCell {

    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var switcher: UISwitch!
    
    private var eventListener: CellEventProtocol?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        self.eventListener = eventListener
        guard let content = content as? (String, Bool) else { return }
        captionLabel.text = content.0
        switcher.setOn(content.1, animated: false)
    }
    
    func updateSwitchValue(enabled: Bool) {
        switcher.setOn(enabled, animated: true)
        if let cellModel = eventListener as? Cell,
            var content = cellModel.content as? (String, Bool) {
            content.1 = enabled
            cellModel.content = content
        }
    }
    
    @IBAction func switched(_ sender: UISwitch) {
        eventListener?.boolValueChanged(sender.isOn)
    }
    
}

extension Cell {
    convenience init(switcherTitle: String, enabled: Bool) {
        self.init(cellType: .switcher)
        self.content = (switcherTitle, enabled)
    }
}
