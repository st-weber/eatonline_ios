//
//  PlaceMenuDS.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 06.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class PlaceMenuDS: DataSource {

    let place: Place
    var startDishType: DishType?
    var startDish: Dish?
    
    var dishTypes: [DishType] = []
    var dishCells: [Int32:IndexPath] = [:]

    var expandedTypeIndex = -1;
    var expandedSubtypeIndex = -1;
    var expandedDishes: [Dish] = []
    
    init(tableInteractor: DataUpdateProtocol, place: Place, startDishType: DishType? = nil, startDish: Dish? = nil) {
        self.place = place
        self.startDishType = startDishType
        self.startDish = startDish
        super.init(tableInteractor: tableInteractor)
        NotificationCenter.subscribe(observer: self, selector: #selector(basketChanged), type: .basketChanged)
        NotificationCenter.subscribe(observer: self, selector: #selector(basketCleared), type: .basketCleared)
        NotificationCenter.subscribe(observer: self, selector: #selector(favouriteDishSwitched), type: .favouriteDishSwitched)
    }
    
    override func prepareData() {
        dataSource = []
        let sectionInfo = Section()
        sectionInfo.cells.append(Cell(placeInfo: place))
        dataSource.append(sectionInfo)
        dishTypes = CoreDataManager.instance.getPlaceDishTypes(Int(place.id))
        for type in dishTypes {
            let typeSection = Section()
            let typeCell = Cell(menuDishType: type, expanded: false)
            typeCell.cellTapped = { indexPath in
                if let indexPath = indexPath {
                    self.typeCellPressed(indexPath.section - 1)
                }
            }
            typeSection.cells.append(typeCell)
            dataSource.append(typeSection)
        }
    }
    
    func checkExpanded() {
        var expandedIndex: Int?
        var startDishSubtype: DishType?
        
        if let startDish = startDish, let dishType = startDish.type {
            if let parentType = dishType.type {
                startDishType = parentType
                startDishSubtype = dishType
            } else {
                startDishType = dishType
            }
        }
        
        if let startDishType = startDishType, startDishType.type == nil {
            for (index, type) in dishTypes.enumerated() {
                if type.id == startDishType.id {
                    expandedIndex = index
                }
            }
        }
        
        if let expandedIndex = expandedIndex {
            typeCellPressed(expandedIndex)
            if let startDishSubtype = startDishSubtype, let startDishType = startDishType {
                let subtypes = CoreDataManager.instance.getTypeSubtypes(dishTypeId: Int(startDishType.id), placeId: Int(place.id))
                for (index, subtype) in subtypes.enumerated() {
                    if subtype.id == startDishSubtype.id {
                        subtypeCellPressed(IndexPath(row: index, section: expandedIndex))
                    }
                }
            }
        }
    }
    
    func typeCellPressed(_ index: Int) {
        
        if expandedTypeIndex == -1 {
            // ничего не развернуто
            expandedTypeIndex = index
            expandType(index)
        } else if index == expandedTypeIndex {
            // нажат раскрытый тип
            closeType(index)
            expandedTypeIndex = -1
            expandedSubtypeIndex = -1
        } else {
            // нажат другой тип
            closeType(expandedTypeIndex)
            expandType(index)
            expandedTypeIndex = index
            expandedSubtypeIndex = -1
        }
    }
    
    func expandType(_ index: Int) {
        let type = dishTypes[index]
        let subtypes = CoreDataManager.instance.getTypeSubtypes(dishTypeId: Int(type.id), placeId: Int(place.id))
        var activeDishPath: IndexPath?
        
        if subtypes.count > 0 {
            var insertPaths: [IndexPath] = []
            for subtype in subtypes {
                let subtypeCell = Cell(menuDishType: subtype, expanded: false)
                subtypeCell.cellTapped = { indexPath in
                    if let indexPath = indexPath {
                        var row = indexPath.row - 1
                        if row > self.expandedSubtypeIndex {
                            row -= self.expandedDishes.count
                        }
                        self.subtypeCellPressed(IndexPath(row: row, section: indexPath.section - 1))
                    }
                }
                insertPaths.append(IndexPath(row: dataSource[index + 1].cells.count, section: index + 1))
                dataSource[index + 1].cells.append(subtypeCell)
            }
            tableInteractor?.insertRows(indexPaths: insertPaths, animation: .fade)
        } else {
            let dishes = CoreDataManager.instance.getTypeDishes(placeId: Int(place.id), dishTypeId: Int(type.id))
            expandedDishes = dishes
            var insertPaths: [IndexPath] = []
            
            for dish in dishes {
                let dishCell = drawDishCell(dish)
                let dishCellPath = IndexPath(row: dataSource[index + 1].cells.count, section: index + 1)
                dishCells[dish.id] = dishCellPath
                insertPaths.append(dishCellPath)
                dataSource[index + 1].cells.append(dishCell)
                if let startDish = startDish, startDish.id == dish.id {
                    activeDishPath = dishCellPath
                }
            }
            tableInteractor?.insertRows(indexPaths: insertPaths, animation: .fade)
        }
        if let activeDishPath = activeDishPath {
            showDish(indexPath: activeDishPath)
        } else {
            tableInteractor?.scrollToRow(IndexPath(row: 0, section: index + 1))
        }
    }
    
    func closeType(_ index: Int) {
        var deletePaths: [IndexPath] = []
        for row in 1...dataSource[index + 1].cells.count - 1 {
            deletePaths.append(IndexPath(row: row, section: index + 1))
        }
        dataSource[index + 1].cells.removeLast(dataSource[index + 1].cells.count - 1)
        expandedDishes = []
        tableInteractor?.deleteRows(indexPaths: deletePaths, animation: .fade)
    }
    
    
    func subtypeCellPressed(_ indexPath: IndexPath) {
        if expandedSubtypeIndex == -1 {
            // ничего не развернуто
            expandedSubtypeIndex = indexPath.row
            expandSubtype(indexPath)
        } else if indexPath.row == expandedSubtypeIndex, indexPath.section == expandedTypeIndex {
            // нажат раскрытый подтип
            closeSubtype(indexPath)
            expandedSubtypeIndex = -1
        } else {
            // нажат другой подтип
            
            closeSubtype(IndexPath(row: expandedSubtypeIndex, section: indexPath.section))
            expandSubtype(indexPath)
            expandedSubtypeIndex = indexPath.row
        }
    }
    
    func expandSubtype(_ indexPath: IndexPath) {
        let type = dishTypes[indexPath.section]
        let subtypes = CoreDataManager.instance.getTypeSubtypes(dishTypeId: Int(type.id), placeId: Int(place.id))
        let subtype = subtypes[indexPath.row]
        
        let dishes = CoreDataManager.instance.getTypeDishes(placeId: Int(place.id), dishTypeId: Int(subtype.id))
        expandedDishes = dishes
        var insertPaths: [IndexPath] = []
        var activeDishPath: IndexPath?
        
        for (index, dish) in dishes.enumerated() {
            let dishCell = drawDishCell(dish)
            let dishCellPath = IndexPath(row: indexPath.row + 2 + index, section: indexPath.section + 1)
            dishCells[dish.id] = dishCellPath
            insertPaths.append(dishCellPath)
            dataSource[indexPath.section + 1].cells.insert(dishCell, at: indexPath.row + 2 + index)
            if let startDish = startDish, startDish.id == dish.id {
                activeDishPath = dishCellPath
            }
        }
        tableInteractor?.insertRows(indexPaths: insertPaths, animation: .fade)
        if let activeDishPath = activeDishPath {
            showDish(indexPath: activeDishPath)
        } else {
            tableInteractor?.scrollToRow(IndexPath(row: indexPath.row + 1, section: indexPath.section + 1))
        }

    }
    
    func showDish(indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.startDish = nil
            self.tableInteractor?.scrollToRow(indexPath)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.tableInteractor?.indicateRow(indexPath)
            }
        }
    }
    
    func closeSubtype(_ indexPath: IndexPath) {
        let type = dishTypes[indexPath.section]
        let subtypes = CoreDataManager.instance.getTypeSubtypes(dishTypeId: Int(type.id), placeId: Int(place.id))
        let subtype = subtypes[indexPath.row]
        let dishes = CoreDataManager.instance.getTypeDishes(placeId: Int(place.id), dishTypeId: Int(subtype.id))
        expandedDishes = []
        var deletePaths: [IndexPath] = []
        for row in indexPath.row + 2 ... indexPath.row + 2 + dishes.count - 1 {
            deletePaths.append(IndexPath(row: row, section: indexPath.section + 1))
            dataSource[indexPath.section + 1].cells.remove(at: indexPath.row + 2)
        }
        tableInteractor?.deleteRows(indexPaths: deletePaths, animation: .fade)
    }
    
    private func drawDishCell(_ dish: Dish) -> Cell {
        let cell = Cell(menuDish: dish, amount: CoreDataManager.instance.getCurrentOrderDishAmount(dish: dish))
        cell.minusPressedEvent = { dish in
            Basket.current.decrementDish(dish)
        }
        cell.plusPressedEvent = { dish in
            Basket.current.incrementDish(dish)
        }
        cell.cellTapped = { indexPath in
            self.showDishDetails(dish)
        }
        return cell
    }
    
    private func showDishDetails(_ currentDish: Dish) {
        for (index, dish) in expandedDishes.enumerated() {
            if currentDish.id == dish.id {
                if #available(iOS 13.0, *), UIDevice.current.userInterfaceIdiom == .phone {
                    Router.shared.presentController(.disheDetails(expandedDishes, index))
                } else {
                    Router.shared.presentController(.disheDetails(expandedDishes, index), root: true, fullScreen: true)
                }
                break
            }
        }
    }
    
    @objc func basketChanged(notification: Notification) {
        if let amount = notification.object as? OrderAmount,
            let dish = amount.dish {
            updateDishCell(dish: dish, amount: amount)
        } else if let dish = notification.object as? Dish {
            updateDishCell(dish: dish, amount: nil)
        }
    }
    
    @objc func basketCleared() {
        for section in dataSource {
            for cell in section.cells {
                if var content = cell.content as? (Dish, OrderAmount?), let indexPath = cell.indexPath {
                    content.1 = nil
                    cell.content = content
                    self.tableInteractor?.updateCellContent(indexPath: indexPath)
                }
            }
        }
    }
    
    @objc func favouriteDishSwitched(notification: Notification) {
        guard let dish = notification.object as? Dish else { return }
        if let indexPath = dishCells[dish.id] {
            tableInteractor?.updateCellContent(indexPath: indexPath)
        }
    }
    
    func updateDishCell(dish: Dish, amount: OrderAmount?) {
        if let indexPath = dishCells[dish.id],
        dataSource[indexPath.section].cells.count > indexPath.row {
            let cellModel = dataSource[indexPath.section].cells[indexPath.row]
            if cellModel.type == .menuDish {
                cellModel.content = (dish, amount)
                tableInteractor?.updateCellContent(indexPath: indexPath)
            }
        }
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
