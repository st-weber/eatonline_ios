//
//  CoreDataManager+OrderAmount.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import CoreData

extension CoreDataManager {
    
    func getOrderAmounts(orderNumber: Int) -> [OrderAmount] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderAmount")
        request.predicate = NSPredicate(format: "order.number = %@", "\(orderNumber)")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let amounts = result as? [OrderAmount] {
                return amounts
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func getCurrentOrderDishAmount(dish: Dish) -> OrderAmount? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderAmount")
        request.predicate = NSPredicate(format: "order.number = 0 AND dish.id = %@", "\(dish.id)")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let amount = result.first as? OrderAmount {
                return amount
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func createAmount(dish: Dish, order: Order? = nil) -> OrderAmount {
        let amount = OrderAmount.init(context: persistentContainer.viewContext)
        amount.dish = dish
        if let order = order {
            amount.order = order
        }
        amount.amount = 1
        saveContext()
        return amount
    }
    
    func deleteAmount(_ amount: OrderAmount) {
        persistentContainer.viewContext.delete(amount)
        saveContext()
    }
    
}
