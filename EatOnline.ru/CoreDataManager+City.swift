//
//  CoreDataManager+City.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import CoreData
import SwiftyJSON

extension CoreDataManager {

    func getCityById(_ id: Int) -> City? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let location = result.first as? City {
                return location
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }

    func getCities(onlyParent: Bool = false) -> [City] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        if onlyParent {
             request.predicate = NSPredicate(format: "parentCity == nil")
        }
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let locations = result as? [City] {
                return locations
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func getCityDistricts(cityId: Int) -> [City] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        request.predicate = NSPredicate(format: "parentCity.id = %@", "\(cityId)")
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let locations = result as? [City] {
                return locations
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func createOrUpdateLocations(json: [JSON]) {
        var oldCities: [Int32: City] = [:]
        for city in getCities() {
            oldCities[city.id] = city
        }
        
        for locationJson in json {
            if let location = createOrUpdateLocation(json: locationJson) {
                oldCities.removeValue(forKey: location.id)
                saveContext()
            }
        }
        
        for oldCity in oldCities.values {
            print("delete oldCity: \(oldCity.name!)")
            persistentContainer.viewContext.delete(oldCity)
        }
        saveContext()
    }
    
    private func createOrUpdateLocation(json: JSON) -> City? {
        guard let dict = json.dictionary,
            let id = dict["id"]?.int,
            let name = dict["name"]?.string else { return nil }
    
        var location: City?
        if let coreLocation = getCityById(id) {
            location = coreLocation
        } else {
            location = City.init(context: persistentContainer.viewContext)
            location?.id = Int32(id)
        }
        location?.name = name
        location?.sort = Int16(dict["sort"]?.int ?? 0)
        
        if let parentId = dict["parent"]?.int, let parentCity = getCityById(parentId) {
            
            location?.parentCity = parentCity
        }
        
        return location
    }
    
}
