//
//  NotificationCenter.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import Foundation

extension NotificationCenter {
    
    static func post(type: NotificationType, object: Any? = nil, userInfo: [AnyHashable : Any]? = nil) {
        NotificationCenter.default.post(name: Notification.Name(type.rawValue), object: object, userInfo: userInfo)
    }
    
    static func subscribe(observer: Any, selector: Selector, type: NotificationType) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: Notification.Name(type.rawValue), object: nil)
    }
    
    static func unsubscribe(_ observer: Any) {
        NotificationCenter.default.removeObserver(observer)
    }
    
}
