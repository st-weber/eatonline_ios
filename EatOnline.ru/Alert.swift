//
//  Alert.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 22.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit


struct Alert {
    
    typealias AlertAction = (title: String, callback: (() -> Void))
    
    static func showError(_ error: Error, ok: (() -> Void)? = nil) {
        var errorText: String = ""
        switch error.localizedDescription {
        case "error_no_connection":
            errorText = "Отсутствует интернет соединение"
        default:
            errorText = error.localizedDescription
        }
        Alert.show(title: "Ошибка", message: errorText, actions: nil, ok: ok)
    }
    
    static func showErrorMessage(_ string: String, ok: (() -> Void)? = nil) {
        Alert.show(title: "Ошибка", message: string, actions: nil, ok: ok)
    }
    
    static func show(title: String, message: String, actions: [AlertAction]?, ok: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            if let actions = actions {
                for action in actions {
                    alert.addAction(UIAlertAction(title: action.title, style: .default, handler: { (_) in
                        action.callback()
                    }))
                }
            } else {
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                    ok?()
                }))
            }
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }

}

