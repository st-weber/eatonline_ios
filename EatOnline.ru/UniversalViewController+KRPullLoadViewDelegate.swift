//
//  UniversalViewController+PullToRefresh.swift
//  Direct Farm
//
//  Created by Michail Solyanic on 20/12/2018.
//  Copyright © 2018 Levon Hovsepyan. All rights reserved.
//

import KRPullLoader

extension UniversalViewController {
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        switch state {
            
        case let .loading(completionHandler):
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                if type == .loadMore {
                    /*if self.loadMoreDelegate?.allowNextPageLoad ?? false {
                        self.loadMoreDelegate?.loadMoreCompletionHandler = completionHandler
                        self.loadMoreDelegate?.loadMore()
                    } else {
                        completionHandler()
                    }*/
                } else {
                    self.refreshDelegate?.refreshCompletionHandler = completionHandler
                    self.refreshDelegate?.refreshPulled()
                }
            }
        default: break
        }
    }
    
    /*
    func refreshSetEnabled(_ enabled: Bool) {
        guard let tableView = tableView else { return }
        if enabled {
            tableView.addPullLoadableView(refreshView, type: .refresh)
        } else {
            tableView.removePullLoadableView(type: .refresh)
        }
    }
    
    func loadMoreSetEnabled(_ enabled: Bool) {
        guard let tableView = tableView else { return }
        if enabled {
            tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        } else {
            tableView.removePullLoadableView(type: .loadMore)
        }
    }*/
}
