//
//  BasketCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class BasketCell: UniversalTableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    
    private weak var eventListener: CellEventProtocol?
    weak var dish: Dish?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let amount = content as? OrderAmount else { return }
        self.dish = amount.dish
        self.eventListener = eventListener
        nameLabel.text = amount.dish?.name
        updateAmount(amount)
    }
    
    func updateAmount(_ amount: OrderAmount) {
        guard let dish = amount.dish else { return }
        priceLabel.text = dish.getPrice(amount: Int(amount.amount))
        weightLabel.text = dish.getWeightText(amount: Int(amount.amount))
        countLabel.text = "x \(amount.amount)"
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        eventListener?.deletePressed()
    }
    
    @IBAction func minusPressed(_ sender: Any) {
        guard let dish = dish else { return }
        eventListener?.minusPressed(dish: dish)
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        guard let dish = dish else { return }
        eventListener?.plusPressed(dish: dish)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
}

extension Cell {
    convenience init(basketItem: OrderAmount) {
        self.init(cellType: .basketCell)
        self.content = basketItem
    }
}
