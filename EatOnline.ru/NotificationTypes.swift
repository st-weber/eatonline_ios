//
//  NotificationTypes.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import Foundation

enum NotificationType: String {
    case startLoadData = "start_load_data"
    case dataLoaded = "data_loaded"
    case startMenuLoading = "start_menu_loading"
    case placesMenuLoaded = "places_menu_loaded"
    case placeMenuLoaded = "place_menu_loaded"
    case cityChanged = "city_changed"
    case basketChanged = "basket_changed"
    case basketCleared = "basket_cleared"
    case deliveryPriceDetected = "delivery_price_detected"
    case addressDetected = "address_detected"
    case orderUpdated = "order_updated"
    
    case userAuthorised = "user_authorised"
    case userUnauthorised = "user_unauthorised"
    
    case favouritePlaceSwitched = "favourite_place_switched"
    case favouriteDishSwitched = "favourite_dish_switched"
    
    case onNotificationReceived = "onNotificationReceived"
    case fcmToken = "FCMToken"
}
