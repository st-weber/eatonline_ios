//
//  ProfileViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class ProfileViewController: UniversalViewController {

    let apiInteractor = ApiInteractor()

    override func viewDidLoad() {
        showBasket = false
        super.viewDidLoad()
        NotificationCenter.subscribe(observer: self, selector: #selector(cityChanged), type: .cityChanged)
        NotificationCenter.subscribe(observer: self, selector: #selector(userAuthorised), type: .userAuthorised)
        NotificationCenter.subscribe(observer: self, selector: #selector(didLogOut), type: .userUnauthorised)
        checkLogoutButton()
        
        showProgress()
        apiInteractor.logIn {
            self.hideProgress()
        }
    }
    
    @objc func cityChanged() {
        self.prepareData()
        updateTable()
        LoadingCoordinator.shared.loadData()
        tabBarController?.selectedIndex = 1
    }
    
    func checkLogoutButton() {
        if ConfigStorage.shared.authHash != nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_logout"), style: .plain, target: self, action: #selector(logOut))
            if dataSource.count > 0, dataSource[0].cells.count > 0 {
                dataSource[0].cells[0] = getOrdersCell()
                self.updateRows(indexPaths: [IndexPath(row: 0, section: 0)], animation: .automatic)
            }
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func userAuthorised() {
        checkLogoutButton()
    }
    
    @objc func logOut() {
        let yesAction = (title: "Да", callback: { () -> Void in
            self.didLogOut()
        })
        let noAction = (title: "Нет", callback: { () -> Void in })
        Alert.show(title: "Выйти из учетной записи?", message: "Вы не сможете просматривать историю заказов", actions: [yesAction, noAction])
    }
    
    @objc func didLogOut() {
        ConfigStorage.shared.authHash = nil
        ConfigStorage.shared.loginPhone = nil
        self.dataSource[0].cells[0] = self.getOrdersCell()
        self.updateRows(indexPaths: [IndexPath(row: 0, section: 0)], animation: .automatic)
        self.checkLogoutButton()
    }
    
     override func prepareData() {
        dataSource = []

        let sectionMe = Section()
        sectionMe.headerProperties.title = "Личные данные"
        
        sectionMe.cells.append(getOrdersCell())
        
        var cityName: String?
        if let currentCityId = ConfigStorage.shared.currentCityId,
            let city = CoreDataManager.instance.getCityById(currentCityId) {
            cityName = city.name
        }
        let cityCell = Cell(propertyTitle: "Город", value: cityName)
        cityCell.cellTapped = { indexPath in
            Router.shared.pushController(.profileCity)
        }
        cityCell.disclosureIndicator = true
        sectionMe.cells.append(cityCell)
        
        let pushesCell = Cell(switcherTitle: "PUSH - уведомления", enabled: ConfigStorage.shared.pushesEnabled)
        pushesCell.boolChangedEvent = { newValue in
            self.switchNotifications(enable: newValue)
        }
        sectionMe.cells.append(pushesCell)
        dataSource.append(sectionMe)
        
        let sectionContacts = Section()
        sectionContacts.headerProperties.title = "Обратная связь"
        
        let hotlineCell = Cell(simpleTitle: "Горячая линия")
        hotlineCell.cellTapped = { indexPath in
            ConfigStorage.shared.hotlinePhone?.call()
        }
        hotlineCell.disclosureIndicator = true
        sectionContacts.cells.append(hotlineCell)
        
        let operatorCell = Cell(simpleTitle: "Что с моим заказом?")
        operatorCell.cellTapped = { indexPath in
            self.showOperatorActions()
        }
        operatorCell.disclosureIndicator = true
        sectionContacts.cells.append(operatorCell)
        
        let qualityCell = Cell(simpleTitle: "Контроль качества")
        qualityCell.cellTapped = { indexPath in
            ConfigStorage.shared.ownerPhone?.call()
        }
        qualityCell.disclosureIndicator = true
        sectionContacts.cells.append(qualityCell)
        
        let wroteUsCell = Cell(simpleTitle: "Написать нам")
        wroteUsCell.cellTapped = { indexPath in
            Router.shared.pushController(.wroteUs)
        }
        wroteUsCell.disclosureIndicator = true
        sectionContacts.cells.append(wroteUsCell)
        dataSource.append(sectionContacts)
        
        if ConfigStorage.shared.infoPages.count > 0 {
            let sectionInfo = Section()
            sectionInfo.headerProperties.title = "Информация"
            for page in ConfigStorage.shared.infoPages {
                let pageCell = Cell(simpleTitle: page.name)
                pageCell.cellTapped = { indexPath in
                    Router.shared.pushController(.textPage(page.name, page.endpoint))
                }
                pageCell.disclosureIndicator = true
                sectionInfo.cells.append(pageCell)
            }
            dataSource.append(sectionInfo)
        }
        updateTable()
    }
        
    func getOrdersCell() -> Cell {
        let value = ConfigStorage.shared.authHash == nil ? "войти" : ""
        let ordersCell = Cell(propertyTitle: "Мои заказы", value: value)
        ordersCell.cellTapped = { indexPath in
            if ConfigStorage.shared.authHash == nil {
                Router.shared.pushController(.profilePhone)
            } else {
                Router.shared.pushController(.myOrders)
            }
        }
        ordersCell.disclosureIndicator = true
        return ordersCell
    }
    
    func switchNotifications(enable: Bool) {
        if enable {
            apiInteractor.pushSubscribe { (error) in
                if let error = error {
                    if let switchCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? SwitchCell {
                        switchCell.updateSwitchValue(enabled: false)
                    }
                    Alert.showError(error)
                } else {
                    print("pushesEnabled")
                    ConfigStorage.shared.pushesEnabled = true
                }
            }
        } else {
            apiInteractor.pushUnubscribe { (error) in
                if let error = error {
                    if let switchCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? SwitchCell {
                        switchCell.updateSwitchValue(enabled: true)
                    }
                    Alert.showError(error)
                } else {
                    print("pushesDisabled")
                    ConfigStorage.shared.pushesEnabled = false
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
