//
//  MSCity+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSCity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSCity (CoreDataProperties)

+ (NSFetchRequest<MSCity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *districts;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSString *parentID;
@property (nonatomic) int16_t sort;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) MSCity *parenCity;
@property (nullable, nonatomic, retain) NSSet<MSPlace *> *places;
@property (nullable, nonatomic, retain) MSSettings *settingsCity;
@property (nullable, nonatomic, retain) NSSet<MSCity *> *subCities;

@end

@interface MSCity (CoreDataGeneratedAccessors)

- (void)addPlacesObject:(MSPlace *)value;
- (void)removePlacesObject:(MSPlace *)value;
- (void)addPlaces:(NSSet<MSPlace *> *)values;
- (void)removePlaces:(NSSet<MSPlace *> *)values;

- (void)addSubCitiesObject:(MSCity *)value;
- (void)removeSubCitiesObject:(MSCity *)value;
- (void)addSubCities:(NSSet<MSCity *> *)values;
- (void)removeSubCities:(NSSet<MSCity *> *)values;

@end

NS_ASSUME_NONNULL_END
