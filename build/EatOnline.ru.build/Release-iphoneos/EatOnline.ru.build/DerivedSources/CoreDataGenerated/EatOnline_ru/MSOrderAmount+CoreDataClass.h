//
//  MSOrderAmount+CoreDataClass.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MSDish, MSOrder;

NS_ASSUME_NONNULL_BEGIN

@interface MSOrderAmount : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MSOrderAmount+CoreDataProperties.h"
