//
//  Place.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import Foundation

extension Place {
    
    var isUnavailable: String? {
        var reason: String?
        if needToUpdate {
            reason = "Загрузка\n меню"
        } else if output {
            reason = "В заведении выходной день"
        }
        // TODO add time checking
        return reason
    }
    
    var isClosed: String? {
        var closed = false
        if (openHour < closeHour) { // работает до полуночи
            if ((openHour <= Date.currentHour) && (closeHour > Date.currentHour)) {
                closed = false
            } else {
                closed = true
            }
        } else if (openHour > closeHour) { // работает после полуночи
            if ((openHour <= Date.currentHour) || (closeHour > Date.currentHour)) {
                closed = false
            } else {
                closed = true
            }
        }
        
        if closed {
            var beforeOpenText = ""
            var hoursForOpen: Int = Int(openHour) + 23 - Date.currentHour;
            if (hoursForOpen > 23) {
                hoursForOpen -= 24
            }
            let minutesForOpen = 59 - Date.currentMinute;
            if (hoursForOpen > 0) {
                beforeOpenText = "До открытия\n\(hoursForOpen)ч. \(minutesForOpen) мин."
            } else {
                beforeOpenText = "До открытия\n\(minutesForOpen) мин."
            }
            return beforeOpenText
        } else {
            return nil
        }
    }
    
    var paymentText: String? {
        var paymentString = "Наличными"
        if courierPayment {
            paymentString += " / картой курьеру"
        }
        if sberbankPayment {
            paymentString += " / сбербанк онлайн"
        }
        return paymentString
    }
    
    var deliveryText: String? {
        var deliveryString = "бесплатно"
        if deliveryPrice > 0 {
            deliveryString = "\(deliveryPrice) р."
        } else if deliveryFrom > 0 {
            deliveryString = "бесплатно, при заказе от \(deliveryFrom) р."
        }
        return "Доставка: " + deliveryString + " (по городу)"
    }
    
    var minimalOrderText: String? {
        return "Минимальный заказ от \(minimalOrder) р."
    }
    
    func switchFavourite() {
        favourite = !favourite
        CoreDataManager.instance.saveContext()
        NotificationCenter.post(type: .favouritePlaceSwitched, object: self)
    }
    
    func setDetectedDeliveryPrice(_ price: String?) {
        deliveryPriceText = price
        CoreDataManager.instance.saveContext()
        NotificationCenter.post(type: .deliveryPriceDetected, object: self)
    }
    
}
