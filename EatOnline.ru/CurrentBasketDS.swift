//
//  CurrentBasketDS.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class CurrentBasketDS: DataSource {

    var cellsMap: [Int32:Int] = [:]
    
    override init(tableInteractor: DataUpdateProtocol) {
        super.init(tableInteractor: tableInteractor)
        NotificationCenter.subscribe(observer: self, selector: #selector(basketChanged), type: .basketChanged)
    }
    
    override func prepareData() {
        dataSource = []
        cellsMap = [:]
        let section = Section()
        for (index, item) in Basket.current.currentOrder.items.enumerated() {
            if let dish = item.dish {
                cellsMap[dish.id] = index
            }
            let itemCell = Cell(basketItem: item)
            itemCell.minusPressedEvent = { dish in
                Basket.current.decrementDish(dish, nonZero: true)
            }
            itemCell.plusPressedEvent = { dish in
                Basket.current.incrementDish(dish)
            }
            itemCell.deletePressedEvent = {
                Basket.current.deleteAmount(item)
            }
            section.cells.append(itemCell)
        }
        dataSource = [section]
    }
    
    @objc func basketChanged(notification: Notification) {
        if let amount = notification.object as? OrderAmount,
            let dish = amount.dish {
            updateDishCell(dish: dish, amount: amount)
        } else if let dish = notification.object as? Dish {
            deleteDishCell(dish: dish)
        }
    }
    
    func updateDishCell(dish: Dish, amount: OrderAmount?) {
        if let index = cellsMap[dish.id] {
            let cellModel = dataSource[0].cells[index]
            cellModel.content = amount
            tableInteractor?.updateCellContent(indexPath: IndexPath(row: index, section: 0))
        }
    }
    
    func deleteDishCell(dish: Dish) {
        if let index = cellsMap[dish.id] {
            dataSource[0].cells.remove(at: index)
            tableInteractor?.deleteRows(indexPaths: [IndexPath(row: index, section: 0)], animation: .automatic)
            cellsMap.removeValue(forKey: dish.id)
            for pos in cellsMap {
                if pos.value > index {
                    cellsMap[pos.key] = pos.value - 1
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
