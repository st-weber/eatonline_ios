//
//  MSSettings+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSSettings+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSSettings (CoreDataProperties)

+ (NSFetchRequest<MSSettings *> *)fetchRequest;

@property (nonatomic) int16_t personalDistrict;
@property (nullable, nonatomic, copy) NSString *personalFloor;
@property (nullable, nonatomic, copy) NSString *personalHouse;
@property (nullable, nonatomic, copy) NSString *personalIntecrom;
@property (nullable, nonatomic, copy) NSString *personalName;
@property (nullable, nonatomic, copy) NSString *personalPhone;
@property (nullable, nonatomic, copy) NSString *personalPorch;
@property (nullable, nonatomic, copy) NSString *personalRoom;
@property (nullable, nonatomic, copy) NSString *personalStreet;
@property (nonatomic) int16_t personsNumber;
@property (nonatomic) BOOL receiveNotifications;
@property (nullable, nonatomic, retain) MSCity *currentCity;

@end

NS_ASSUME_NONNULL_END
