//
//  PushNotificationCenter.swift
//  Direct Farm
//
//  Created by Levon Hovsepyan on 10/26/18.
//  Copyright © 2018 Levon Hovsepyan. All rights reserved.
//

import Foundation
import UserNotifications
import Firebase
import FirebaseMessaging
import NotificationCenter
import SwiftyJSON

protocol PushNotificationCenterDelegate: class {
    func didReceive(firebase token: String)
    func didReceive(firebase push: PushNotification, isRuntime: Bool)
}

class PushNotificationHelper {
    static func getPushNotification(userInfo: [AnyHashable: Any]) -> PushNotification? {
        let json = JSON(userInfo)
        guard let push = PushNotification(json: json) else {
            return nil
        }
        return push
    }
}


class PushNotificationCenter: NSObject {
    
    // MARK: - Main
    
    weak var delegate: PushNotificationCenterDelegate?
    static let shared = PushNotificationCenter()
    
    private override init() { }
    
    // MARK: - Methods
    
    func dispatchAtAppStartUp(_ launchOptions: [AnyHashable: Any]?) {
        let notificationKey = UIApplication.LaunchOptionsKey.remoteNotification
        guard let notifJSON = launchOptions?[notificationKey] as? [String: Any] else { return }
        if let push = PushNotificationHelper.getPushNotification(userInfo: notifJSON) {
            delegate?.didReceive(firebase: push, isRuntime: false)
        }
    }
    
    func showWhenAppRunning(_ pushNotificationJson: [AnyHashable: Any]) {
        guard let push = PushNotificationHelper.getPushNotification(userInfo: pushNotificationJson) else { return }
        let type: PushNotificationOptionType = push.eventType
        switch type {
        
        case .orderProcessed:
            NotificationCenter.post(type: .orderUpdated, object: (push.serviceContent, "accepted"))
        case .orderCanceled:
            NotificationCenter.post(type: .orderUpdated, object: (push.serviceContent, "canceled"))
        /*case .newMessage:
            NotificationCenter.post(type: .onResponseCreated)
        case .editMessage:
            NotificationCenter.post(type: .onResponseUpdated)*/
        default:
            break
        }
    }
    
    func dispatchWhenAppRunning(_ pushNotificationJson: [AnyHashable: Any]) {
        guard let push = PushNotificationHelper.getPushNotification(userInfo: pushNotificationJson) else { return }
        delegate?.didReceive(firebase: push, isRuntime: UIApplication.shared.applicationState.rawValue == 0)
    }
    
    func registerForPush() {
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (status, error) in
            if let error = error {
                print(error)
            } else {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    NotificationCenter.default.addObserver(self, selector: #selector(self.connectToFcm), name: Notification.Name.InstanceIDTokenRefresh, object: nil)
                    self.connectToFcm()
                }
            }
        }
    }
    
    @objc private func connectToFcm() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Firebase>>> \(error)")
            } else if let result = result {
                self.delegate?.didReceive(firebase: result.token)
            }
        }
    }
    
}
