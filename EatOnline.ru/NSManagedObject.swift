//
//  NSManagedObject.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 07.04.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject {
    
    func save() {
        CoreDataManager.instance.saveContext()
    }
    
}
