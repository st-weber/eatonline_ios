//
//  BillViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class BillViewController: UniversalViewController {
    
    override func prepareData() {
        tableView.contentInset = UIEdgeInsets(top: 5.0, left: 0, bottom: 5.0, right: 0)
        tableView.separatorStyle = .none
        let section = Section()
        for action in CoreDataManager.instance.getActions(placeId: nil) {
            let actionCell = Cell(action: action)
            actionCell.cellTapped = { indexPath in
                guard let place = action.place else { return }
                if !place.needToUpdate {
                    Router.shared.pushController(.place(place, nil, nil))
                }
            }
            section.cells.append(actionCell)
        }
        dataSource = [section]
        updateTable()
    }
    
}
