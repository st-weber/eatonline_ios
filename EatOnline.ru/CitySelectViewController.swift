//
//  CitySelectViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 24.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class CitySelectViewController: UniversalViewController {

    override func viewDidLoad() {
        self.showBasket = false
        super.viewDidLoad()
    }
    
    override func prepareData() {
        guard let currentCityId = ConfigStorage.shared.currentCityId else { return }
        
        let section = Section()
        
        for city in CoreDataManager.instance.getCities(onlyParent: true) {
            let cityCell = Cell(city: city, checked: city.id == currentCityId)
            cityCell.cellTapped = { indexPath in
                self.cityPressed(Int(city.id))
            }
            section.cells.append(cityCell)
            
            for district in CoreDataManager.instance.getCityDistricts(cityId: Int(city.id)) {
                let districtCell = Cell(city: district, checked: district.id == currentCityId)
                districtCell.cellTapped = { indexPath in
                    self.cityPressed(Int(district.id))
                }
                section.cells.append(districtCell)
            }
        }
        
        dataSource = [section]
        updateTable()
    }
    
    func cityPressed(_ cityId: Int) {
        if ConfigStorage.shared.currentCityId != cityId {
            if let oldCityId = ConfigStorage.shared.currentCityId {
                CoreDataManager.instance.deletePlaces(currentCityId: oldCityId)
            }
            Basket.current.clear()
            NotificationCenter.post(type: .basketChanged)
            ConfigStorage.shared.currentCityId = cityId
            ConfigStorage.shared.currentCityKladr = nil
            NotificationCenter.post(type: .cityChanged)
        }
        self.goBack()
    }
    
}
