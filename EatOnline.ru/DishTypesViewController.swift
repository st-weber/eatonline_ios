//
//  DishTypesViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class DishTypesViewController: UniversalViewController {

    override func viewDidLoad() {
        self.showSearch = true
        super.viewDidLoad()
        NotificationCenter.subscribe(observer: self, selector: #selector(showProgress), type: .startMenuLoading)
        NotificationCenter.subscribe(observer: self, selector: #selector(placesLoaded), type: .placesMenuLoaded)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if LoadingCoordinator.shared.isLoadingMenu {
            showProgress()
        }
    }
    
    override func prepareData() {
        let section = Section()
        for type in CoreDataManager.instance.getDishTypes(forMainScreen: true) {
            let typeCell = Cell(dishType: type)
            typeCell.cellTapped = { indexPath in
                Router.shared.pushController(.dishTypePlaces(type))
            }
            section.cells.append(typeCell)
        }
        dataSource = [section]
        updateTable()
    }
    
    @objc func placesLoaded() {
        hideProgress()
        prepareData()
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
