//
//  UIColor.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 12/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var appRed: UIColor {
        return UIColor(red: 231.0 / 255.0, green: 76.0 / 255.0, blue: 60.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appLightGray: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 246.0 / 255.0, blue: 246.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var grayButton: UIColor {
        return UIColor(red: 208.0 / 255.0, green: 211.0 / 255.0, blue: 212.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appLightText: UIColor {
        return UIColor(white: 80.0 / 255.0, alpha: 0.5)
    }
    
    @nonobjc class var appText: UIColor {
        return UIColor(white: 80.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appBlack: UIColor {
        return UIColor(white: 40.0 / 255.0, alpha: 1.0)
    }


    @nonobjc class var alphaWhite: UIColor {
        return UIColor(white: 1.0, alpha: 0.5)
    }
    
}
