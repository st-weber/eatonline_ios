//
//  Router.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class Router {

    static let shared : Router = Router()
    var window: UIWindow
    weak var presentedController: UIViewController?

    init() {
        if let w = UIApplication.shared.delegate?.window, let window = w {
            self.window = window
        } else {
            self.window = UIWindow(frame: UIScreen.main.bounds)
        }
        self.window.makeKeyAndVisible()
    }
    
    func startApp() {
        window.rootViewController = ViewControllerFactory.start.create
    }
 
    func showMain() {
        let main = ViewControllerFactory.tabBarController.create
        changeRootController(main)
        if let tabController = main as? TabBarViewController {
            tabController.checkActionsTab()
            if tabController.selectedIndex == 0 {
                tabController.selectedIndex = 1
            }
        }
        LoadingCoordinator.shared.loadData()
    }
    
    func pushController(_ type: ViewControllerFactory) {
        let viewController = type.create
        if let tabBarController = UIApplication.shared.keyWindow?.rootViewController as? TabBarViewController,
            let currentNavigationController = tabBarController.viewControllers?[tabBarController.selectedIndex] as? NavigationController {
            if let presentedNavigationController = presentedController as? NavigationController {
                presentedNavigationController.pushViewController(viewController, animated: true)
            } else {
                viewController.hidesBottomBarWhenPushed = true
                currentNavigationController.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func presentController(_ type: ViewControllerFactory, root: Bool = false, fullScreen: Bool = false) {
        let vc = type.create
        if root {
            let navController = NavigationController(rootViewController: vc)
            if fullScreen {
                navController.modalPresentationStyle = .fullScreen
            }
            presentController(navController)
        } else {
            if fullScreen {
                vc.modalPresentationStyle = .fullScreen
            }
            presentController(vc)
        }
    }
    
    func presentController(_ viewController: UIViewController, root: Bool = false) {
        if let tabBarController = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController,
            let currentNavigationController = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
            currentNavigationController.present(viewController, animated: true, completion: nil)
            presentedController = viewController
        }
    }
    
    private func changeRootController(_ newController: UIViewController) {
        UIView.transition(with: window, duration: 0.2, options: .transitionCrossDissolve, animations: {
            let oldState: Bool = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            self.window.rootViewController = newController
            UIView.setAnimationsEnabled(oldState)
        }, completion: nil)
    }
    
}
