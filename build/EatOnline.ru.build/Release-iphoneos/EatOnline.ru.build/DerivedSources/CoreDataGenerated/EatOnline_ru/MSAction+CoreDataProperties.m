//
//  MSAction+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSAction+CoreDataProperties.h"

@implementation MSAction (CoreDataProperties)

+ (NSFetchRequest<MSAction *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSAction"];
}

@dynamic identifier;
@dynamic imageHeight;
@dynamic imagePath;
@dynamic imageWidth;
@dynamic onMainScreen;
@dynamic title;
@dynamic type;
@dynamic place;

@end
