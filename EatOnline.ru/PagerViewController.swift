//
//  PagerViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 21.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

enum PagerMode {
    case top
    case bottom
}

class PagerViewController: UniversalViewController {

    @IBOutlet var pagerView: UIView!
    @IBOutlet var pagerViewHeight: NSLayoutConstraint!
    @IBOutlet var indicatorView: UIView!
    @IBOutlet var contentView: UIScrollView!
    @IBOutlet var bottomPagerLabel: UILabel!
    
    var viewControllers: [UniversalViewController]
    var currentPage: Int
    var newPage: Int
    var buttonAnimation = false
    let mode: PagerMode
    private var lastContentOffset: CGFloat = 0.0
    
    init(viewControllers: [UniversalViewController], currentPage: Int = 0, mode: PagerMode) {
        self.viewControllers = viewControllers
        self.currentPage = currentPage
        self.newPage = currentPage
        self.mode = mode
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawPages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for viewController in viewControllers {
            viewController.viewWillAppear(animated)
        }
    }

    func drawPages() {
        guard viewControllers.count > 0 else { return }
        
        if mode == .top {
            pagerView.isHidden = false
            pagerViewHeight.constant = 30.0
            indicatorView.isHidden = false
            bottomPagerLabel.isHidden = true
            let tabWidth = Screen.width / CGFloat(viewControllers.count)
            pagerView.clearSubviews()
            for (index, viewController) in viewControllers.enumerated() {
                let tabButton = UIButton(frame: CGRect(x: CGFloat(index) * tabWidth, y: 0, width: tabWidth, height: pagerView.bounds.height))
                tabButton.setTitle(viewController.title, for: .normal)
                tabButton.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
                tabButton.setTitleColor((index == currentPage) ? .appText : .appLightText, for: .normal)
                tabButton.tag = index
                tabButton.addTarget(self, action: #selector(tabButtonPressed(_:)), for: .touchDown)
                pagerView.addSubview(tabButton)
                viewController.view.frame = CGRect(x: Screen.width * CGFloat(index), y: 0, width: contentView.bounds.width, height: contentView.bounds.height)
                contentView.addSubview(viewController.view)
            }
            indicatorView.frame = CGRect(x: CGFloat(currentPage) * tabWidth, y: indicatorView.frame.origin.y, width: tabWidth, height: indicatorView.frame.height)
        } else {
            view.backgroundColor = .appBlack
            contentView.backgroundColor = .clear
            pagerView.isHidden = true
            pagerViewHeight.constant = 0.0
            indicatorView.isHidden = true
            bottomPagerLabel.isHidden = false
            for (index, viewController) in viewControllers.enumerated() {
                viewController.view.frame = CGRect(x: Screen.width * CGFloat(index), y: 0, width: contentView.bounds.width, height: contentView.bounds.height)
                contentView.addSubview(viewController.view)
            }
            bottomPagerLabel.text = "\(currentPage + 1) из \(viewControllers.count)"
        }
        if currentPage > 0 {
            scrollToPage(currentPage)
        }
        contentView.contentSize = CGSize(width: Screen.width * CGFloat(viewControllers.count), height: contentView.contentSize.height)
    }
    
    @objc func tabButtonPressed(_ tabButton: UIButton) {
        guard currentPage != tabButton.tag else { return }
        scrollToPage(tabButton.tag)
    }
    
    func scrollToPage(_ index: Int) {
        setActiveTabButton(index)
        contentView.setContentOffset(CGPoint(x: CGFloat(index) * Screen.width, y: 0.0), animated: true)
    }
    
    func setActiveTabButton(_ index: Int) {
        if mode == .top {
            if let oldButton = pagerView.subviews[currentPage] as? UIButton {
                oldButton.setTitleColor(.appLightText, for: .normal)
            }
            if let newButton = pagerView.subviews[index] as? UIButton {
                newButton.setTitleColor(.appText, for: .normal)
            }
            UIView.animate(withDuration: 0.3) {
                self.indicatorView.frame = CGRect(x: CGFloat(index) * self.indicatorView.frame.width,
                                                  y: self.indicatorView.frame.origin.y,
                                                  width: self.indicatorView.frame.width,
                                                  height: self.indicatorView.frame.height)
            }
        } else {
            bottomPagerLabel.text = "\(index + 1) из \(viewControllers.count)"
        }
        currentPage = index
    }
    
}

extension PagerViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !buttonAnimation, viewControllers.count > 0 else { return }
        indicatorView.frame = CGRect(x: scrollView.contentOffset.x / CGFloat(viewControllers.count),
                                    y: self.indicatorView.frame.origin.y,
                                    width: self.indicatorView.frame.width,
                                    height: self.indicatorView.frame.height)
        newPage = currentPage
        for i in 0...viewControllers.count {
            if (scrollView.contentOffset.x + Screen.width / 2) > CGFloat(i) * Screen.width {
                newPage = i
            }
            
        }
        lastContentOffset = scrollView.contentOffset.x
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard newPage != currentPage, !decelerate, !buttonAnimation else { return }
        setActiveTabButton(newPage)
        lastContentOffset = scrollView.contentOffset.x
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard newPage != currentPage, !buttonAnimation else { return }
        setActiveTabButton(newPage)
        lastContentOffset = scrollView.contentOffset.x
    }
        
}
