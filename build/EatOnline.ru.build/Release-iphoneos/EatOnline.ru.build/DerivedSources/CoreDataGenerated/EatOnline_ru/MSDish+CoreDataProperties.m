//
//  MSDish+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSDish+CoreDataProperties.h"

@implementation MSDish (CoreDataProperties)

+ (NSFetchRequest<MSDish *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSDish"];
}

@dynamic favourite;
@dynamic identifier;
@dynamic imagePath;
@dynamic price;
@dynamic serviceDelivery;
@dynamic serviceReservation;
@dynamic serviceTakeaway;
@dynamic sort;
@dynamic title;
@dynamic weight;
@dynamic weightDetails;
@dynamic weightUnits;
@dynamic orderAmount;
@dynamic place;
@dynamic type;

@end
