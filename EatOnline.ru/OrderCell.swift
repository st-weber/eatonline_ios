//
//  OrderCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 20.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class OrderCell: UniversalTableViewCell {

    @IBOutlet var deviceIcon: UIImageView!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var placeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var arrow: UIImageView!
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let content = content as? (Order, Bool) else { return }
        deviceIcon.backgroundColor = content.0.statusColor
        deviceIcon.image = content.0.deviceImage
        numberLabel.text = String(content.0.number)
        placeLabel.text = " - " + (content.0.placeName ?? "")
        dateLabel.text = content.0.date
        arrow.rotate(angle: content.1 ? CGFloat.pi : 0)
    }
    
}

extension Cell {
    convenience init(order: Order, expanded: Bool) {
        self.init(cellType: .order)
        self.content = (order, expanded)
    }
}
