//
//  SearchViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 21.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class SearchViewController: UniversalViewController {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchTable: TableView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    var callback: ((Any)->())?
    
    lazy var searchDS: SearchDS = {
        return SearchDS(tableInteractor: self)
    }()
    
    override func viewDidLoad() {
        self.showBasket = false
        self.customTableView = searchTable
        self.DS = searchDS
        self.keyboardManager.delegate = self
        super.viewDidLoad()
        searchBar.setTextColor(color: .black)
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchDS.searchUpdated(searchText)
    }
    
}

extension SearchViewController: KeyboardEventsDelegate {
    
    func keyboardWillShow(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat) {
        bottomConstraint.constant = keyboardHeight
        UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func keyboardWillHide(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}
