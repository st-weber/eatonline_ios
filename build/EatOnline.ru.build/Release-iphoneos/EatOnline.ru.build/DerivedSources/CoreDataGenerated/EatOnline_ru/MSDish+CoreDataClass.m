//
//  MSDish+CoreDataClass.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSDish+CoreDataClass.h"
#import "MSDishType+CoreDataClass.h"

#import "MSOrderAmount+CoreDataClass.h"

#import "MSPlace+CoreDataClass.h"

@implementation MSDish

@end
