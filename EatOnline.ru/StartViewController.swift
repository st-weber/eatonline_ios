//
//  StartViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class StartViewController: UniversalViewController {

    @IBOutlet weak var selectCityLabel: UILabel!
    @IBOutlet weak var selectCityTable: TableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    let apiInteractor = ApiInteractor()
    
    override func viewDidLoad() {
        self.customTableView = selectCityTable
        super.viewDidLoad()
        tableView.backgroundColor = .clear
        selectCityLabel.isHidden = true
        selectCityTable.isHidden = true
        selectCityTable.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 50, right: 0)
        
        if ConfigStorage.shared.currentCityId != nil {
            Router.shared.showMain()
        } else {
            loadCities {
                self.indicator.stopAnimating()
                self.updateData()
                self.selectCityLabel.isHidden = false
                self.selectCityTable.isHidden = false
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }

    override func updateData() {
        let section = Section()
        for city in CoreDataManager.instance.getCities(onlyParent: true) {
            let cityCell = Cell(city: city)
            cityCell.cellTapped = { indexPath in
                self.cityPressed(Int(city.id))
            }
            section.cells.append(cityCell)
            
            for district in CoreDataManager.instance.getCityDistricts(cityId: Int(city.id)) {
                let districtCell = Cell(city: district)
                districtCell.cellTapped = { indexPath in
                    self.cityPressed(Int(district.id))
                }
                section.cells.append(districtCell)
            }
        }
        dataSource = [section]
        updateTable()
    }
    
    func loadCities(loaded: (()->())?) {
        apiInteractor.loadCities {
            loaded?()
        }
    }
        
    func cityPressed(_ cityId: Int) {
        ConfigStorage.shared.currentCityId = cityId
        Router.shared.showMain()
    }
    
    
}
