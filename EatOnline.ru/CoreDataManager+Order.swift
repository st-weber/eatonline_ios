//
//  CoreDataManager+Order.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import CoreData
import SwiftyJSON

extension CoreDataManager {
    
    func getCurrentOrder() -> Order {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Order")
        request.predicate = NSPredicate(format: "number = %@", "0")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let order = result.first as? Order {
                return order
            } else {
                let order = Order.init(context: persistentContainer.viewContext)
                order.number = 0
                saveContext()
                return order
            }
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    func getOrderById(_ id: Int) -> Order? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Order")
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let order = result.first as? Order {
                return order
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func clearOrders() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Order")
        request.predicate = NSPredicate(format: "number != %@", "0")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let orders = result as? [Order] {
                for order in orders {
                    persistentContainer.viewContext.delete(order)
                }
                saveContext()
                return
            }
        } catch {
            return
        }
    }
    
    func createOrUpdateOrdersList(json: [JSON]) -> [Order] {
        clearOrders()
        var orders: [Order] = []
        for orderJson in json {
            if let order = createOrUpdateOrder(json: orderJson) {
                orders.append(order)
            }
        }
        saveContext()
        return orders
    }
    
    
    private func createOrUpdateOrder(json: JSON) -> Order? {
        guard let dict = json.dictionary,
            let id = dict["id"]?.int else { return nil }
    
        var order: Order?
        if let coreOrder = getOrderById(id) {
            order = coreOrder
        } else {
            order = Order.init(context: persistentContainer.viewContext)
            order?.id = Int32(id)
        }
        
        order?.number = Int32(dict["number"]?.int ?? 0)
        order?.date = dict["date"]?.string
        order?.status = dict["status"]?.string
        order?.finalPrice = Int16(dict["price"]?.int ?? 0)
        order?.address = dict["address"]?.string
        order?.placeName = dict["place_name"]?.string
        if let placeId = dict["place"]?.int {
            order?.place = getPlaceById(placeId)
        }
        order?.persons = Int16(dict["persons"]?.int ?? 0)
        order?.type = Int16(dict["type"]?.int ?? 0)
        order?.forTime = dict["for_time"]?.string
        order?.device = dict["device"]?.string
        
        if let dishesArr = dict["dishes"]?.array {
            for dishJson in dishesArr {
                if let dishId = dishJson.dictionary?["id"]?.int,
                    let dish = getDishById(dishId),
                    let amount = dishJson.dictionary?["amount"]?.int {
                    let newAmount = CoreDataManager.instance.createAmount(dish: dish, order: order)
                    newAmount.amount = Int16(amount)
                    newAmount.order = order
                }
            }
        }
        
        return order
    }
}
