//
//  MenuDishCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 05/11/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class MenuDishCell: UniversalTableViewCell {
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var dishImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var detailsLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var dweightLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var minusButton: UIButton!
    @IBOutlet var plusButton: UIButton!
    @IBOutlet var orderButton: UIButton!
    @IBOutlet var favouriteButton: UIButton!
    
    private weak var eventListener: CellEventProtocol?
    weak var dish: Dish?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let content = content as? (Dish, OrderAmount?) else { return }
        self.dish = content.0
        self.eventListener = eventListener
        indicator.startAnimating()
        dishImage.setImage(path: content.0.imagePath) {
            self.indicator.stopAnimating()
        }
        nameLabel.text = content.0.name
        detailsLabel.text = content.0.details?.trimmed
        weightLabel.text = content.0.getWeightText()
        dweightLabel.text = content.0.weightDetails
        priceLabel.text = content.0.getPrice()
        
        updateAmount(content.1)
        updateFavouriteButton()
    }
    
    func updateAmount(_ amount: OrderAmount?) {
        if let amount = amount {
            countLabel.isHidden = false
            countLabel.text = "x \(amount.amount)"
            orderButton.isHidden = true
            minusButton.isHidden = false
            plusButton.isHidden = false
        } else {
            countLabel.isHidden = true
            orderButton.isHidden = false
            minusButton.isHidden = true
            plusButton.isHidden = true
        }
    }
    
    func animateFinded() {
        
        let theAnimation = CABasicAnimation(keyPath: "transform.scale")
        theAnimation.duration = 0.4
        //theAnimation.beginTime = CACurrentMediaTime() + 0.5
        theAnimation.repeatCount = 3
        theAnimation.autoreverses = true
        theAnimation.fromValue = 1.0
        theAnimation.toValue = 1.15
        dishImage.layer.add(theAnimation, forKey: "scale")
    }
    
    @IBAction func favouritePressed(_ sender: UIButton) {
        dish?.switchFavourite()
        updateFavouriteButton()
    }
    
    func updateFavouriteButton() {
        guard let dish = dish else { return }
        favouriteButton.setImage(UIImage(named: dish.favourite ? "favourite_active" : "favourite_passive"), for: .normal)
    }
    
    @IBAction func minusPressed(_ sender: Any) {
        guard let dish = dish else { return }
        eventListener?.minusPressed(dish: dish)
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        guard let dish = dish else { return }
        eventListener?.plusPressed(dish: dish)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
}

extension Cell {
    convenience init(menuDish: Dish, amount: OrderAmount?) {
        self.init(cellType: .menuDish)
        self.content = (menuDish, amount)
    }
}
