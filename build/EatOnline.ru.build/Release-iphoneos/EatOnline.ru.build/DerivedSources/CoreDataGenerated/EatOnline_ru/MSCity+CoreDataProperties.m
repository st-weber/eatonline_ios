//
//  MSCity+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSCity+CoreDataProperties.h"

@implementation MSCity (CoreDataProperties)

+ (NSFetchRequest<MSCity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSCity"];
}

@dynamic districts;
@dynamic identifier;
@dynamic parentID;
@dynamic sort;
@dynamic title;
@dynamic parenCity;
@dynamic places;
@dynamic settingsCity;
@dynamic subCities;

@end
