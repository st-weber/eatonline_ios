//
//  DishDetailsViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 17.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class DishDetailsViewController: UniversalViewController {

    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var dishImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var dweightLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var minusButton: UIButton!
    @IBOutlet var plusButton: UIButton!
    @IBOutlet var orderButton: UIButton!
    @IBOutlet var favouriteButton: UIButton!
    
    let dish: Dish
    
    init(dish: Dish) {
        self.dish = dish
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        indicator.startAnimating()
        dishImage.setImage(path: dish.bigImagePath) {
            self.indicator.stopAnimating()
        }
        nameLabel.text = dish.name
        weightLabel.text = dish.getWeightText()
        dweightLabel.text = dish.weightDetails
        priceLabel.text = dish.getPrice()
        descriptionLabel.text = dish.details?.trimmed
        
        updateAmount(CoreDataManager.instance.getCurrentOrderDishAmount(dish: dish))
        updateFavouriteButton()
        
        NotificationCenter.subscribe(observer: self, selector: #selector(basketChanged), type: .basketChanged)
    }

    func updateAmount(_ amount: OrderAmount?) {
        if let amount = amount {
            countLabel.isHidden = false
            countLabel.text = "x \(amount.amount)"
            orderButton.isHidden = true
            minusButton.isHidden = false
            plusButton.isHidden = false
        } else {
            countLabel.isHidden = true
            orderButton.isHidden = false
            minusButton.isHidden = true
            plusButton.isHidden = true
        }
    }
    
    @IBAction func favouritePressed(_ sender: UIButton) {
        dish.switchFavourite()
        updateFavouriteButton()
    }
    
    func updateFavouriteButton() {
        favouriteButton.setImage(UIImage(named: dish.favourite ? "detail_favourite_active" : "detail_favourite_passive"), for: .normal)
    }
    
    @IBAction func minusPressed(_ sender: Any) {
        Basket.current.decrementDish(dish)
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        Basket.current.incrementDish(dish)
    }
    
    @objc func basketChanged(notification: Notification) {
        if let amount = notification.object as? OrderAmount,
            let amountDish = amount.dish, amountDish.id == dish.id {
            updateAmount(amount)
        } else if let notificationDish = notification.object as? Dish, notificationDish.id == dish.id {
            updateAmount(nil)
        }
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
