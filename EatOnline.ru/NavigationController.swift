//
//  NavigationController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    var goBackPressed: (() -> ())?
    
    override func viewDidLoad() {
        navigationBar.isTranslucent = false
        navigationBar.barTintColor = UIColor.appRed
        navigationBar.tintColor = UIColor.white
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white , NSAttributedString.Key.font: UIFont.duke(size: 28)]
        view.backgroundColor = .white
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let topVC = viewControllers.last {
            return topVC.preferredStatusBarStyle
        }
        return .default
    }
    
}
