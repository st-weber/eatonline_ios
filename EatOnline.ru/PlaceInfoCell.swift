//
//  PlaceInfoCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 06.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class PlaceInfoCell: UniversalTableViewCell {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var kitchensLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var deliveryPriceButton: UIButton!
    @IBOutlet weak var minimalLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    
    weak var place: Place?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let place = content as? Place else { return }
        self.place = place
        indicator.startAnimating()
        backgroundImage.setImage(path: place.backgroundPath, completed: {
            self.indicator.stopAnimating()
        })
        logoImage.setImage(path: place.logoPath)
        timeLabel.text = place.workTime
        kitchensLabel.text = place.kitchens
        addressLabel.text = place.address
        if let deliveryPrice = place.deliveryPriceText, deliveryPrice.count > 0 {
            deliveryLabel.text = "Стоимость доставки: \(deliveryPrice)"
        } else {
            deliveryLabel.text = "Стоимость доставки: не определена"
        }
        minimalLabel.text = place.minimalOrderText
        paymentLabel.text = place.paymentText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
    @IBAction func deliveryPricePressed(_ sender: Any) {
        guard let place = place else { return }
        var address: String?
        if let currentAddress = Basket.current.currentAddress {
            address = currentAddress
        } else if let savedAddress = ConfigStorage.shared.myAddress {
            address = savedAddress
        }
        Router.shared.presentController(.addressPicker(place, address), root: true)
    }
    
}

extension Cell {
    convenience init(placeInfo: Place) {
        self.init(cellType: .placeInfo)
        self.content = placeInfo
    }
}
