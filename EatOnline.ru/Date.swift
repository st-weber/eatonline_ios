//
//  Date.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import Foundation

extension Date {
    
    static var currentHour: Int {
        return Calendar.current.component(.hour, from: Date())
    }
    
    static var currentMinute: Int {
        return Calendar.current.component(.minute, from: Date())
    }
        
}
