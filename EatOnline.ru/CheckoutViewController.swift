//
//  CheckoutViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 17.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit
import InputMask
import KMPlaceholderTextView

class CheckoutViewController: UniversalViewController, MaskedTextFieldDelegateListener {

    var maskDelegate = MaskedTextFieldDelegate(primaryFormat: String.phoneMask)
    
    var personsCount: Int = 0
    var agreementChecked = false
    
    @IBOutlet var nameField: UITextField!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var phoneCheckedImage: UIImageView!
    @IBOutlet var deliveryButton: UIButton!
    @IBOutlet var takeawayButton: UIButton!
    @IBOutlet var serviceTypeViewLeft: NSLayoutConstraint!
    @IBOutlet var serviceTypeViewRight: NSLayoutConstraint!
    
    @IBOutlet var addressView: UIView!
    @IBOutlet var addressHeight: NSLayoutConstraint!
    @IBOutlet var addressButton: UIButton!
    
    @IBOutlet var entranceHeight: NSLayoutConstraint!
    @IBOutlet var entranceCaption: UILabel!
    @IBOutlet var entranceField: UITextField!
    @IBOutlet var intercomCaption: UILabel!
    @IBOutlet var intercomField: UITextField!
    
    @IBOutlet var floorCaption: UILabel!
    @IBOutlet var floorHeight: NSLayoutConstraint!
    @IBOutlet var floorField: UITextField!
    @IBOutlet var roomCaption: UILabel!
    @IBOutlet var roomField: UITextField!
    
    @IBOutlet var deliveryTimeLabel: UILabel!
    @IBOutlet var deliveryTimeField: UITextField!
    @IBOutlet var timePicker: UIDatePicker!
    
    @IBOutlet var personsField: UILabel!
    @IBOutlet var changeField: UITextField!
    let changePickerValues = ["не требуется", "c 500р.", "c 1000р.", "c 2000р.", "c 5000р."]
    
    @IBOutlet var paymentField: UITextField!
    @IBOutlet var commentField: KMPlaceholderTextView!
    
    @IBOutlet var orderPriceLabel: UILabel!
    @IBOutlet var deliveryPriceCaption: UILabel!
    @IBOutlet var deliveryPriceTop: NSLayoutConstraint!
    @IBOutlet var deliveryPriceLabel: UILabel!
    @IBOutlet var deliveryPriceButton: UIButton!
    @IBOutlet var agreementCheckButton: UIButton!
    @IBOutlet var orderButton: UIButton!
    
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        self.showBasket = false
        self.keyboardManager.delegate = self
        super.viewDidLoad()

        setupFields()
        
        serviceChanged(delivery: true)
        setCheckoutEnabled(agreementChecked)
        
        NotificationCenter.subscribe(observer: self, selector: #selector(setAddress), type: .addressDetected)
        NotificationCenter.subscribe(observer: self, selector: #selector(deliveryPriceDetected), type: .deliveryPriceDetected)
    }
    
    func setupFields() {
        maskDelegate.listener = self
        phoneField.delegate = maskDelegate
        phoneField.placeholder = String.phonePlaceholder
        commentField.textContainerInset = UIEdgeInsets.init(top: -2, left: -5, bottom: 0.0, right: 0.0)
        
        nameField.text = ConfigStorage.shared.myName
        phoneField.text = ConfigStorage.shared.loginPhone
        
        entranceField.text = ConfigStorage.shared.myEntrance
        floorField.text = ConfigStorage.shared.myFloor
        intercomField.text = ConfigStorage.shared.myFloor
        roomField.text = ConfigStorage.shared.myRoom
        
        timePicker.minimumDate = Date()
        deliveryTimeField.inputView = timePicker
        
        orderPriceLabel.text = "\(Basket.current.currentOrder.price) р."
        
        entranceCaption.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captionPressed(_ :))))
        intercomCaption.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captionPressed(_ :))))
        floorCaption.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captionPressed(_ :))))
        roomCaption.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captionPressed(_ :))))
    }
        
    @objc private func captionPressed(_ recognizer : UITapGestureRecognizer) {
        guard let index = recognizer.view?.tag else { return }
        switch index {
        case 0:
            entranceField.becomeFirstResponder()
        case 1:
            intercomField.becomeFirstResponder()
        case 2:
            floorField.becomeFirstResponder()
        case 3:
            roomField.becomeFirstResponder()
        default:
            break
        }
    }
    
    @IBAction func phoneFieldFocused(_ sender: UITextField) {
        if let phone = sender.text, phone.isValidPhoneNumber && ConfigStorage.shared.authHash != nil {
            let yesAction = (title: "Да", callback: { () -> Void in
                self.didLogOut()
            })
            let noAction = (title: "Нет", callback: { () -> Void in })
            Alert.show(title: "Выйти из текущей учетной записи?", message: "Вы не сможете просматривать историю заказов", actions: [yesAction, noAction])
        }
    }
    
    @objc func didLogOut() {
        ConfigStorage.shared.authHash = nil
        ConfigStorage.shared.loginPhone = nil
        phoneField.text = nil
    }
    
    
    @objc func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        //checkSendButton()
    }
    
    @objc func deliveryPriceDetected() {
        setAddress()
        setDeliveryPrice()
    }
    
    @IBAction func nameChanged(_ sender: UITextField) {
        
    }
    
    @IBAction func deliveryPressed(_ sender: Any) {
        serviceChanged(delivery: true)
        Basket.current.currentOrder.type = 0
        Basket.current.currentOrder.save()
    }
    
    @IBAction func takeawayPressed(_ sender: Any) {
        serviceChanged(delivery: false)
        Basket.current.currentOrder.type = 1
        Basket.current.currentOrder.save()
    }
    
    func serviceChanged(delivery: Bool) {
        if delivery {
            serviceTypeViewLeft.constant = 15
            serviceTypeViewRight.constant = Screen.width - deliveryButton.frame.width
            deliveryTimeLabel.text = "Доставить к:"
            deliveryTimeField.placeholder = "ближайшему времени"
            
            addressView.isHidden = false
            addressHeight.constant = 41
            entranceHeight.constant = 41
            floorHeight.constant = 41
                        
            setAddress()
            setDeliveryPrice()
        } else {
            serviceTypeViewLeft.constant = deliveryButton.frame.width
            serviceTypeViewRight.constant = Screen.width - deliveryButton.frame.width - takeawayButton.frame.width
            deliveryTimeLabel.text = "Заберу в:"
            deliveryTimeField.placeholder = "ближайшее время"
            
            addressView.isHidden = true
            addressHeight.constant = 0
            entranceHeight.constant = 0
            floorHeight.constant = 0
            
            setAddress()
            setDeliveryPrice(noDelivery: true)
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func setAddress() {
        if let currentAddress = Basket.current.currentAddress {
            addressButton.setTitle(currentAddress, for: .normal)
            addressButton.setTitleColor(.appText, for: .normal)
        } else if let savedAddress = ConfigStorage.shared.myAddress {
            addressButton.setTitle(savedAddress, for: .normal)
            addressButton.setTitleColor(.appText, for: .normal)
        } else {
            addressButton.setTitle("не указан", for: .normal)
            addressButton.setTitleColor(.lightGray, for: .normal)
        }
    }
    
    func setDeliveryPrice(noDelivery: Bool = false) {
        if noDelivery {
            deliveryPriceCaption.isHidden = true
            deliveryPriceLabel.isHidden = true
            deliveryPriceButton.isHidden = true
            deliveryPriceTop.constant = -16.0
        } else {
            deliveryPriceCaption.isHidden = false
            deliveryPriceTop.constant = 15.0
            if let deliveryPrice = Basket.current.currentOrder.place?.deliveryPriceText {
                deliveryPriceLabel.text = deliveryPrice
                deliveryPriceLabel.isHidden = false
                deliveryPriceButton.isHidden = true
            } else {
                deliveryPriceLabel.isHidden = true
                deliveryPriceButton.isHidden = false
            }
        }
        
    }
        
    @IBAction func addressPressed(_ sender: UIButton) {
        guard let place = Basket.current.currentOrder.place else { return }
        var address: String?
        if let currentAddress = Basket.current.currentAddress {
            address = currentAddress
        } else if let savedAddress = ConfigStorage.shared.myAddress {
            address = savedAddress
        }
        Router.shared.presentController(.addressPicker(place, address), root: true)
    }
    
    @IBAction func timeChanged(_ sender: UIDatePicker) {
        if sender.date.timeIntervalSince1970 > Date().timeIntervalSince1970 + 600 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"//   dd.MM.yyyy"
            deliveryTimeField.text = dateFormatter.string(from: sender.date)
        } else {
            deliveryTimeField.text = nil
        }
    }
    
    @IBAction func plusMinusPressed(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if personsCount > 0 {
                personsCount -= 1
            }
        } else {
            personsCount += 1
        }
        personsField.text = "Количество приборов: \(personsCount == 0 ? "?" : String(personsCount))"
        sender.selectedSegmentIndex = -1
    }
    
    @IBAction func changePressed(_ sender: Any) {
        showPickerView(values: changePickerValues, current: changeField.text) { (newValue, index) in
            if index == 0 {
                self.changeField.text = nil
            } else {
                self.changeField.text = newValue
            }
        }
    }
    
    @IBAction func paymentPressed(_ sender: Any) {
        guard let place = Basket.current.currentOrder.place else { return }
        var values = ["наличными"]
        if place.onlinePayment {
            values.append("онлайн")
        }
        if place.courierPayment {
            values.append("картой курьеру")
        }
        if place.sberbankPayment {
            values.append("сбербанк онлайн")
        }
        showPickerView(values: values, current: paymentField.text) { (newValue, index) in
            if index == 0 {
                self.paymentField.text = nil
            } else {
                self.paymentField.text = newValue
            }
        }
    }
    
    @IBAction func agreementPressed(_ sender: UIButton) {
        agreementChecked = !agreementChecked
        sender.setImage(UIImage(named: agreementChecked ? "privacy_checked" : "privacy_unchecked"), for: .normal)
        setCheckoutEnabled(agreementChecked)
    }
    
    @IBAction func readAgreementPressed(_ sender: UIButton) {
        for page in ConfigStorage.shared.infoPages {
            if page.name.contains("соглашение") {
                Router.shared.pushController(.textPage(page.name, page.endpoint))
                break
            }
        }
    }
    
    func setCheckoutEnabled(_ enabled: Bool) {
        orderButton.isEnabled = enabled
        orderButton.backgroundColor = enabled ? .appRed : .appLightText
    }
    
    @IBAction func orderPressed(_ sender: Any) {
        saveOrderProperties()
        guard let phone = phoneField.text else { return }
        if phone.isValidPhoneNumber {
            if ConfigStorage.shared.authHash != nil {
                let apiInteractor = ApiInteractor()
                showProgress()
                apiInteractor.logIn {
                    apiInteractor.createOrder { (orderNumber) in
                        self.hideProgress()
                        if let orderNumber = orderNumber {
                            Basket.current.clear()
                            NotificationCenter.post(type: .basketChanged)
                            Router.shared.pushController(.orderCreated(orderNumber))
                        }
                    }
                }
            } else {
                Router.shared.pushController(.checkPhone(phone, false))
            }
        } else {
            Alert.showErrorMessage("Укажите номер телефона")
        }
    }
    
    func saveOrderProperties() {
        ConfigStorage.shared.myName = nameField.text
        ConfigStorage.shared.myAddress = addressButton.titleLabel?.text
        ConfigStorage.shared.myEntrance = entranceField.text
        ConfigStorage.shared.myFloor = floorField.text
        ConfigStorage.shared.myFloor = intercomField.text
        ConfigStorage.shared.myRoom = roomField.text

        var address = addressButton.titleLabel?.text ?? ""
        if let entrance = entranceField.text, entrance.count > 0 {
            address += ", под. \(entrance)"
        }
        if let floor = floorField.text, floor.count > 0 {
            address += ", этаж \(floor)"
        }
        if let intercom = intercomField.text, intercom.count > 0 {
            address += ", домофон \(intercom)"
        }
        if let room = roomField.text, room.count > 0 {
            address += ", кв. \(room)"
        }
        
        
        let order = Basket.current.currentOrder
        order.address = address
        order.persons = Int16(personsCount)
        order.forTime = deliveryTimeField.text
        order.changeFrom = changeField.text
        order.paymentType = paymentField.text
        order.save()
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}

extension CheckoutViewController: KeyboardEventsDelegate {
    
    func keyboardWillShow(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat) {
        bottomConstraint.constant = keyboardHeight
        UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func keyboardWillHide(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.view.layoutIfNeeded()
        }, completion: nil)
    }
}
