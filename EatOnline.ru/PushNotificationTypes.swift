//
//  PushNotificationTypes.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 20.04.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

enum PushNotificationOptionType: String {
    
    case newPlaceMessage = "new_place"
    case placeAdvertMessage = "place_advert"
    case orderProcessed = "order_processed"
    case orderCanceled = "order_canceled"
    case unknown = ""
    
}
