//
//  Endpoints.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/09/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import Alamofire
import Foundation

enum Endpoint {

    case locations
    case dishTypes
    case actions(Int)
    case placesList(Int)
    case placeDishes(Int)
    
    case configSettings
    case textPage(String)
    case wroteUs
    case kladrSuggest(String)
    case addressSuggest(String)
    case addressDetect(String, String)
    
    case requestCode
    case checkCode
    case silentLogin
    case createOrder
    case myOrders
    
    case pushSubscribe
    case pushUnsubscribe
    
    var method: HTTPMethod {
        switch self {
        case .wroteUs:
            return .post
        default:
            return .get
        }
    }
    
    var url: URL {
        switch self {
        case .locations:
            return URL(fromEndpoint: "locations.php")!
        case .dishTypes:
            return URL(fromEndpoint: "dishes.php?req=types")!
        case .actions(let locationId):
            return URL(fromEndpoint: "actions.php?location=\(locationId)")!
        case .placesList(let locationId):
            return URL(fromEndpoint: "places.php?req=list&location=\(locationId)")!
        case .placeDishes(let placeId):
            return URL(fromEndpoint: "dishes.php?req=list&place=\(placeId)")!
        case .configSettings:
            return URL(fromEndpoint: "config.php?type=settings")!
        case .textPage(let endpoint):
            return URL(fromEndpoint: endpoint)!
        case .wroteUs:
            return URL(fromEndpoint: "contact.php")!
        case .kladrSuggest(let cityName):
            return URL(fromEndpoint: "address.php?req=kladr&city=\(cityName.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)")!
        case .addressSuggest(let address):
            return URL(fromEndpoint: "address.php?req=suggest&address=\(address.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)")!
        case .addressDetect(let lat, let lon):
            return URL(fromEndpoint: "address.php?req=geolocate&lat=\(lat.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)&lon=\(lon.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)")!
            
        case .requestCode:
            return URL(fromEndpoint: "auth.php?req=code")!
        case .checkCode:
            return URL(fromEndpoint: "auth.php?req=check_code")!
        case .silentLogin:
            return URL(fromEndpoint: "auth.php?req=login")!
        case .createOrder:
            return URL(fromEndpoint: "orders.php?req=create")!
        case .myOrders:
            return URL(fromEndpoint: "orders.php?req=list")!
            
        case .pushSubscribe:
            return URL(fromEndpoint: "push.php?req=subscribe")!
        case .pushUnsubscribe:
            return URL(fromEndpoint: "push.php?req=unsubscribe")!
        }
    }
    
}
