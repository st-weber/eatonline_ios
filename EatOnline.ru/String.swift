//
//  String.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 11/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

extension String {
    
    var htmlDecoded: String {
        let decoded = try? NSAttributedString(data: Data(utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ], documentAttributes: nil).string
        return decoded ?? self
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    static var phoneMask: String {
        return "+7 ([000]) [000]-[00]-[00]"
    }
    
    static var codeMask: String {
        return "[0000]"
    }
    
    static var phonePlaceholder: String {
        return "+7 (___) ___-__-__"
    }
    
    var isValidPhoneNumber: Bool {
        return count >= 18
    }
    
    func formatPhone() -> String {
        var phone = self
        if phone.first == "8" {
            phone.insert("-", at: phone.index(phone.startIndex, offsetBy: 10))
            phone.insert("-", at: phone.index(phone.startIndex, offsetBy: 7))
            phone.insert(" ", at: phone.index(phone.startIndex, offsetBy: 4))
            phone.insert(" ", at: phone.index(phone.startIndex, offsetBy: 1))
        } else {
            phone.insert("-", at: phone.index(phone.startIndex, offsetBy: 9))
            phone.insert("-", at: phone.index(phone.startIndex, offsetBy: 7))
            phone.insert(" ", at: phone.index(phone.startIndex, offsetBy: 4))
            phone.insert(")", at: phone.index(phone.startIndex, offsetBy: 4))
            phone.insert("(", at: phone.index(phone.startIndex, offsetBy: 1))
            phone.insert(" ", at: phone.index(phone.startIndex, offsetBy: 1))
            phone.insert("+", at: phone.startIndex)
        }
        return phone
    }
    
    func call() {
        let phone = (self.first == "7" ? "+" : "") + self
        if let url = NSURL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    func whatsApp() {
        let phone = (self.first == "7" ? "+" : "") + self
        if let url = URL(string: "https://api.whatsapp.com/send?phone=\(phone)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    
    
}
