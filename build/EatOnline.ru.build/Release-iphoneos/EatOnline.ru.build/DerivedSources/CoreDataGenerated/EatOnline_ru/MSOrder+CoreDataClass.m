//
//  MSOrder+CoreDataClass.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSOrder+CoreDataClass.h"
#import "MSOrderAmount+CoreDataClass.h"

#import "MSPlace+CoreDataClass.h"

#import "MSTable+CoreDataClass.h"

@implementation MSOrder

@end
