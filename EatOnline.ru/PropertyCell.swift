//
//  PropertyCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class PropertyCell: UniversalTableViewCell {

    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    private var eventListener: CellEventProtocol?
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        self.eventListener = eventListener
        guard let content = content as? (String?, String?, Bool) else { return }
        captionLabel.text = content.0
        valueLabel.text = content.1
        showLoading(content.2)
    }
    
    func showLoading(_ show: Bool) {
        if show {
            valueLabel.isHidden = true
            indicator.startAnimating()
        } else {
            valueLabel.isHidden = false
            indicator.stopAnimating()
        }
        if let cellModel = eventListener as? Cell,
            var content = cellModel.content as? (String?, String?, Bool) {
            content.2 = show
            cellModel.content = content
        }
    }

}

extension Cell {
    convenience init(propertyTitle: String?, value: String?, showLoading: Bool = false) {
        self.init(cellType: .property)
        self.content = (propertyTitle, value, showLoading)
    }
}
