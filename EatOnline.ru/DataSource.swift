//
//  DataSource.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 06/11/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

protocol DataSourceProtocol: class {
    var dataSource: [Section] { get }
    func prepareData()
}

protocol DataUpdateProtocol: class {
    func updateTable()
    func updateSections(sections: IndexSet, animation: UITableView.RowAnimation?)
    func updateCellContent(indexPath: IndexPath)
    func updateRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation?)
    func insertRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation?)
    func deleteRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation?)
    func scrollToRow(_ indexPath: IndexPath)
    func indicateRow(_ indexPath: IndexPath)
}

extension DataUpdateProtocol {
    func indicateRow(_ indexPath: IndexPath) {}
}

class DataSource: DataSourceProtocol {

    var dataSource: [Section] = []
    func prepareData() {}
    
    weak var tableInteractor: DataUpdateProtocol?
    
    init(tableInteractor: DataUpdateProtocol) {
        self.tableInteractor = tableInteractor
        self.prepareData()
    }
    
    @objc func updateData() {
        self.prepareData()
    }
    
}
