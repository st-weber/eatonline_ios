//
//  UniversalViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit
import KRPullLoader
import MBProgressHUD

protocol UniversalViewControllerRefreshing: class {
    func refreshPulled()
    var refreshCompletionHandler: (()->Void)? { get set }
}

class UniversalViewController: UIViewController, DataSourceProtocol, HideKeyboardDelegate, KRPullLoadViewDelegate {

    weak var refreshDelegate: UniversalViewControllerRefreshing?
    var refreshView: KRPullLoadView?
    
    var dataSource: [Section] = []
    var DS: DataSourceProtocol?
    weak var customTableView: TableView?
    var hud: MBProgressHUD = MBProgressHUD()
    
    var keyboardManager = KeyboardManager()
    var hideKeyboardByTouchView: UIView?
    
    var hiddenPickerTextView = UITextView()
    var pickerValues: [String]?
    var pickerValueSelected: ((String, Int)->())?
    
    var showBasket = true
    var showSearch = false
    var searchButton: UIBarButtonItem?
    
    lazy var tableView: TableView = {
        
        var delegate: DataSourceProtocol = self
        if let DS = DS {
            delegate = DS
        }
        
        if let customTableView = customTableView {
            customTableView.setupWithCustomView(dataSourceDelegate: delegate)
            return customTableView
        } else {
            return TableView(dataSourceDelegate: delegate, frame: self.view.bounds)
        }
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rightButtons: [UIBarButtonItem] = []
        if showBasket {
            rightButtons.append(BasketButton(badgeNumber: Basket.current.currentOrder.items.count))
        }
        if showSearch {
            searchButton = UIBarButtonItem(image: UIImage(named: "nav_search"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showSearchPage))
            if LoadingCoordinator.shared.isLoadingMenu || LoadingCoordinator.shared.isLoading {
                searchButton?.isEnabled = false
            }
            rightButtons.append(searchButton!)
        }
        navigationItem.rightBarButtonItems = rightButtons
        
        if let navigationController = navigationController {
            if navigationController.viewControllers.count == 1 && tabBarController == nil {
                addCancelButton()
            } else if navigationController.viewControllers.count > 1 {
                addBackButton()
            }
        }
        
        if DS == nil {
            DS = self
            tableView.DS?.prepareData()
        }
        
        if let dataSource = DS?.dataSource, dataSource.count > 0 && customTableView == nil {
            view.addSubview(tableView)
            tableView.translatesAutoresizingMaskIntoConstraints = false
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
            tableView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: 0).isActive = true
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
            tableView.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: 0).isActive = true
        }
        
        if refreshDelegate != nil {
            refreshView = KRPullLoadView()
            refreshView?.delegate = self
            tableView.addPullLoadableView(refreshView!, type: .refresh)
        }
        
        tableView.backgroundColor = .white
        
        NotificationCenter.subscribe(observer: self, selector: #selector(showProgress), type: .startLoadData)
        NotificationCenter.subscribe(observer: self, selector: #selector(hideProgress), type: .dataLoaded)
        NotificationCenter.subscribe(observer: self, selector: #selector(placesMenuLoaded), type: .placesMenuLoaded)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if LoadingCoordinator.shared.isLoading {
            showProgress()
        }
        if LoadingCoordinator.shared.isLoadingMenu || LoadingCoordinator.shared.isLoading {
            searchButton?.isEnabled = false
        }
        keyboardManager.beginMonitoring()
        if keyboardManager.touchDelegate == nil {
            keyboardManager.touchDelegate = self
            hideKeyboardByTouchView = self.view
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        keyboardManager.stopMonitoring()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func prepareData() {
        dataSource = []
    }
    
    func updateData() {
        // to implement
    }
    
    @objc func showProgress() {
        tableView.isUserInteractionEnabled = false
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Загрузка"
    }
    
    @objc func hideProgress() {
        DispatchQueue.main.async {
            self.tableView.isUserInteractionEnabled = true
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            self.updateData()
        }
    }
    
    @objc func placesMenuLoaded() {
        searchButton?.isEnabled = true
    }
    
    @objc func showSearchPage() {
        let callback: ((Any)->())? = { result in
            
        }
        Router.shared.pushController(.search(callback))
    }
    
    @objc func showOperatorActions() {
        var alertStyle = UIAlertController.Style.actionSheet
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertStyle = UIAlertController.Style.alert
        }
        let actionSheet = UIAlertController(title: "Что с моим заказом?", message: nil, preferredStyle: alertStyle)
        
        let phoneAction = UIAlertAction(title: "Звонок оператору", style: .default, handler: { (alert: UIAlertAction!) -> Void in
          ConfigStorage.shared.operatorPhone?.call()
        })
        actionSheet.addAction(phoneAction)
        
        let whatsAppAction = UIAlertAction(title: "WhatsApp - чат", style: .default, handler: { (alert: UIAlertAction!) -> Void in
          ConfigStorage.shared.operatorPhone?.whatsApp()
        })
        actionSheet.addAction(whatsAppAction)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        actionSheet.addAction(cancelAction)
        
        /*
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            presenter.permittedArrowDirections = []
        }*/
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
}

extension UniversalViewController: DataUpdateProtocol {
    
    func updateTable() {
        tableView.reloadData()
    }
    
    func updateSections(sections: IndexSet, animation: UITableView.RowAnimation?) {
        tableView.beginUpdates()
        tableView.reloadSections(sections, with: animation ?? .automatic)
        tableView.endUpdates()
    }
    
    func updateCellContent(indexPath: IndexPath) {
        if tableView.indexPathsForVisibleRows?.contains(indexPath) ?? false {
            guard let cellModel = self.DS?.dataSource[indexPath.section].cells[indexPath.row],
                  let cell = self.tableView.cellForRow(at: indexPath) as? CellContentProtocol else {
                           return
                   }
            cell.fillWithContent(content: cellModel.content, eventListener: cellModel.eventListener)
        }
    }
    
    func updateRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation?) {
        tableView.beginUpdates()
        tableView.reloadRows(at: indexPaths, with: animation ?? .automatic)
        tableView.endUpdates()
    }
    
    func insertRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation?) {
        tableView.beginUpdates()
        tableView.insertRows(at: indexPaths, with: animation ?? .automatic)
        tableView.endUpdates()
    }
    
    func deleteRows(indexPaths: [IndexPath], animation: UITableView.RowAnimation?) {
        tableView.beginUpdates()
        tableView.deleteRows(at: indexPaths, with: animation ?? .automatic)
        tableView.endUpdates()
    }
    
    func scrollToRow(_ indexPath: IndexPath) {
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func indicateRow(_ indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenuDishCell {
            cell.animateFinded()
        }
    }
    
}

extension UniversalViewController {
    
    func showPickerView(values: [String], current: String? = nil, selected: ((String, Int)->())?) {
        pickerValues = values
        pickerValueSelected = selected
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        
        if let current = current, let selectedIndex = values.firstIndex(of: current) {
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        hiddenPickerTextView.inputView = picker
        hiddenPickerTextView.removeFromSuperview()
        self.view.addSubview(hiddenPickerTextView)
        hiddenPickerTextView.becomeFirstResponder()
    }
    
}

extension UniversalViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValues?[row] ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let newTextValue = pickerValues?[row] {
            pickerValueSelected?(newTextValue, row)
        }
    }
    
}
