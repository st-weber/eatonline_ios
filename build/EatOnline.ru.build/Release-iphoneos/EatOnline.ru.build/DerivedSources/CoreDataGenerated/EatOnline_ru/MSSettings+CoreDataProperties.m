//
//  MSSettings+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSSettings+CoreDataProperties.h"

@implementation MSSettings (CoreDataProperties)

+ (NSFetchRequest<MSSettings *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSSettings"];
}

@dynamic personalDistrict;
@dynamic personalFloor;
@dynamic personalHouse;
@dynamic personalIntecrom;
@dynamic personalName;
@dynamic personalPhone;
@dynamic personalPorch;
@dynamic personalRoom;
@dynamic personalStreet;
@dynamic personsNumber;
@dynamic receiveNotifications;
@dynamic currentCity;

@end
