//
//  TabBarViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    convenience init(currentPage: Int) {
        self.init()
        viewControllers = [
            NavigationController(rootViewController: ViewControllerFactory.bill.create),
            NavigationController(rootViewController: ViewControllerFactory.places.create),
            NavigationController(rootViewController: ViewControllerFactory.dishes.create),
            NavigationController(rootViewController: ViewControllerFactory.favourites.create),
            NavigationController(rootViewController: ViewControllerFactory.profile.create)]
        selectedIndex = currentPage
        
        tabBar.shadowImage = UIImage()
        tabBar.tintColor = .white
        tabBar.unselectedItemTintColor = .alphaWhite
        tabBar.barTintColor = .appRed
        tabBar.isTranslucent = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PushNotificationCenter.shared.registerForPush()
        NotificationCenter.subscribe(observer: self, selector: #selector(checkActionsTab), type: .dataLoaded)
    }
    
    @objc func checkActionsTab() {
        guard let viewControllers = viewControllers, viewControllers.count > 1 else { return }
        let enable = CoreDataManager.instance.getActions().count > 0
        tabBar.items?[0].isEnabled = enable
        /*if selectedIndex == 0 {
            selectedIndex = 1
        }*/
    }
    
}
