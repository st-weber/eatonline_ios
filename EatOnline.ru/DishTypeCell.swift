//
//  DishTypeCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class DishTypeCell: UniversalTableViewCell {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var placesCount: UILabel!
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let type = content as? DishType else { return }
        indicator.startAnimating()
        picture.setImage(path: type.imagePath) {
            self.indicator.stopAnimating()
        }
        title.text = type.name
        
        var placesWord = "заведений"
        let count = type.places?.count ?? 0
        if (count % 10 == 1) {
            placesWord = "заведение"
        } else if (count % 10 > 1 && count % 10 < 5) {
            placesWord = "заведения"
        }
        placesCount.text = "\(count) \(placesWord)"
    }
    
}

extension Cell {
    convenience init(dishType: DishType) {
        self.init(cellType: .dishType)
        self.content = dishType
    }
}
