//
//  MSDishType+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSDishType+CoreDataProperties.h"

@implementation MSDishType (CoreDataProperties)

+ (NSFetchRequest<MSDishType *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSDishType"];
}

@dynamic category;
@dynamic iconData;
@dynamic iconPath;
@dynamic identifier;
@dynamic imageData;
@dynamic imagePath;
@dynamic sort;
@dynamic title;
@dynamic dishes;
@dynamic places;
@dynamic subtypes;
@dynamic type;

@end
