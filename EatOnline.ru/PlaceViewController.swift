//
//  PlaceViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 17.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class PlaceViewController: UniversalViewController {

    var place: Place
    var startType: DishType?
    var startDish: Dish?
    
    @IBOutlet var menuTable: TableView!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var minimalLabel: UILabel!
    @IBOutlet var arrowImage: UIImageView!
    @IBOutlet var checkoutButton: UIButton!
    @IBOutlet var bottomLineHeight: NSLayoutConstraint!
    
    init(place: Place) {
        self.place = place
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.customTableView = menuTable
        self.DS = PlaceMenuDS(tableInteractor: self, place: place, startDishType: startType, startDish: startDish)
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItems = [
            BasketButton(badgeNumber: Basket.current.currentOrder.items.count),
            UIBarButtonItem(image: UIImage(named: "nav_address"), style: .plain, target: self, action: #selector(setAddress))]
        
        updateTable()
        minimalLabel.text = "мин. заказ: \(place.minimalOrder)р."
        checkSummary()
        NotificationCenter.subscribe(observer: self, selector: #selector(checkSummary), type: .basketChanged)
        NotificationCenter.subscribe(observer: self, selector: #selector(checkSummary), type: .basketCleared)
        NotificationCenter.subscribe(observer: self, selector: #selector(deliveryPriceDetected), type: .deliveryPriceDetected)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let menuDS = self.DS as? PlaceMenuDS {
            menuDS.checkExpanded()
        }
    }
    
    @objc func setAddress() {
        var address: String?
        if let currentAddress = Basket.current.currentAddress {
            address = currentAddress
        } else if let savedAddress = ConfigStorage.shared.myAddress {
            address = savedAddress
        }
        Router.shared.presentController(.addressPicker(place, address), root: true)
    }
    
    @objc func checkSummary() {
        if let basketPlace = Basket.current.currentOrder.place, basketPlace.id == place.id {
            let summary = Basket.current.currentOrder.price
            summaryLabel.text = "Заказ на сумму: \(summary)р."
            if Basket.current.currentOrder.items.count == 0 {
                if bottomLineHeight.constant > 0 {
                    bottomLineHeight.constant = 0.0
                    UIView.animate(withDuration: 0.1) {
                        self.view.layoutIfNeeded()
                    }
                }
            } else if summary <= Int(place.minimalOrder) {
                arrowImage.isHidden = true
                checkoutButton.isHidden = true
                minimalLabel.isHidden = false
                if bottomLineHeight.constant != 20.0 {
                    bottomLineHeight.constant = 20.0
                    UIView.animate(withDuration: 0.1) {
                        self.view.layoutIfNeeded()
                    }
                }
            } else {
                arrowImage.isHidden = false
                checkoutButton.isHidden = false
                minimalLabel.isHidden = true
                if bottomLineHeight.constant < 44 {
                    bottomLineHeight.constant = 44.0
                    UIView.animate(withDuration: 0.1) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        } else {
            bottomLineHeight.constant = 0.0
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func deliveryPriceDetected() {
        updateRows(indexPaths: [IndexPath(row: 0, section: 0)], animation: .automatic)
    }
    
    @IBAction func checkoutPressed(_ sender: Any) {
        if let closedText = Basket.current.currentOrder.place?.isClosed {
            Alert.show(title: "Заведение закрыто", message: closedText, actions: nil)
            return
        }
        Router.shared.pushController(.checkout)
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }

}
