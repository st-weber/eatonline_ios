//
//  CoreDataManager+Places.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import CoreData
import SwiftyJSON

extension CoreDataManager {

    func getPlaceById(_ id: Int, context: NSManagedObjectContext? = nil) -> Place? {
        var currentContext = persistentContainer.viewContext
        if let context = context {
            currentContext = context
        }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Place")
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        do {
            let result = try currentContext.fetch(request)
            if let place = result.first as? Place {
                return place
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func getPlaces(currentCityId: Int, dishType: DishType? = nil, favourites: Bool = false, needToUpdate: Bool = false) -> [Place] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Place")
        if let dishType = dishType {
            request.predicate = NSPredicate(format: "ANY dishTypes.id == %@ AND city.id == %@", "\(dishType.id)", "\(currentCityId)")
        } else if favourites {
            request.predicate = NSPredicate(format: "city.id = %@ AND favourite == YES", "\(currentCityId)")
        } else if needToUpdate {
            request.predicate = NSPredicate(format: "city.id = %@ AND needToUpdate == YES", "\(currentCityId)")
        } else {
            request.predicate = NSPredicate(format: "city.id = %@", "\(currentCityId)")
        }
        
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let places = result as? [Place] {
                return places
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func searchPlacesByName(_ searchText: String) -> [Place] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Place")
        request.predicate = NSPredicate(format: "name contains[cd] %@", searchText)
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let places = result as? [Place] {
                return places
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func deletePlaces(currentCityId: Int) {
        for place in getPlaces(currentCityId: currentCityId) {
            persistentContainer.viewContext.delete(place)
        }
        saveContext()
    }
    
    func createOrUpdatePlacesList(currentCityId: Int, json: [JSON]) {
        guard let currentCity = getCityById(currentCityId) else { return }
        
        var premiumsCount = 0
        let premiumPositions = 3
        var places: [Place] = []
        
        var oldPlaces: [Int32: Place] = [:]
        for place in getPlaces(currentCityId: currentCityId) {
            oldPlaces[place.id] = place
        }
        
        for placeJson in json {
            if let place = createOrUpdatePlace(currentCity: currentCity, json: placeJson) {
                if placeJson.dictionary?["premium"]?.bool ?? false, premiumsCount < premiumPositions {
                    place.sort = Int16(Int.random(in: 1...premiumPositions))
                    premiumsCount += 1
                } else {
                    place.sort = Int16(Int.random(in: (premiumPositions + 1)...json.count + premiumPositions))
                }
                oldPlaces.removeValue(forKey: place.id)
                places.append(place)
                saveContext()
            }
        }
        
        if premiumsCount < premiumPositions {
            for _ in 1...(premiumPositions - premiumsCount) {
                places[Int.random(in: premiumsCount..<(places.count - premiumsCount))].sort = Int16(Int.random(in: 1...premiumPositions))
            }
        }
        
        for oldPlace in oldPlaces.values {
            print("delete oldPlace: \(oldPlace.name!)")
            persistentContainer.viewContext.delete(oldPlace)
        }
        saveContext()
    }
    
    private func createOrUpdatePlace(currentCity: City, json: JSON) -> Place? {
        guard let dict = json.dictionary,
            let id = dict["id"]?.int,
            let name = dict["name"]?.string else { return nil }
    
        var place: Place?
        if let corePlace = getPlaceById(id) {
            place = corePlace
        } else {
            place = Place.init(context: persistentContainer.viewContext)
            place?.id = Int32(id)
        }
        
        place?.name = name.htmlDecoded
        place?.type = dict["type"]?.string
        place?.kitchens = dict["kitchens"]?.string
        place?.address = dict["address"]?.string
        place?.deliveryPrice = Int16(dict["dprice"]?.int ?? 0)
        place?.deliveryFrom = Int16(dict["dfrom"]?.int ?? 0)
        place?.minimalOrder = Int16(dict["morder"]?.int ?? 0)
        place?.workTime = dict["wtime"]?.string
        place?.openHour = Int16(dict["ophour"]?.int ?? 0)
        place?.closeHour = Int16(dict["clhour"]?.int ?? 0)
        place?.logoPath = dict["logo"]?.string
        place?.backgroundPath = dict["back"]?.string
        place?.coordinates = dict["coordinates"]?.string
        place?.output = dict["output"]?.bool ?? false
        place?.onlinePayment = dict["payment"]?.bool ?? false
        place?.sberbankPayment = dict["sber"]?.bool ?? false
        place?.courierPayment = dict["cterminal"]?.bool ?? false
        place?.footNote = dict["footnote"]?.string
        place?.city = currentCity
        place?.deliveryPriceText = nil
                
        if let currentLastUpdate = dict["updated"]?.string {
            if let savedLastUpdate = place?.lastUpdate, savedLastUpdate == currentLastUpdate {
                if let dishes = place?.dishes, dishes.count > 0 {
                    place?.needToUpdate = false
                } else {
                    place?.needToUpdate = true
                }
            } else {
                place?.lastUpdate = currentLastUpdate
                place?.needToUpdate = true
            }
        } else {
            place?.needToUpdate = true
        }
        
        return place
    }
    
}
