//
//  Order.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

extension Order {
    
    var price: Int {
        var price = 0
        for amount in items {
            guard let dish = amount.dish else { continue }
            price += Int(dish.price) * Int(amount.amount)
        }
        return price
    }
    
    var items: [OrderAmount] {
        return CoreDataManager.instance.getOrderAmounts(orderNumber: Int(number))
    }
    
    var statusTitle: String {
        switch status {
        case "accepted", "performed":
            return "Выполняется"
        case "canceled":
            return "Отменен"
        default:
            return "Обрабатывается"
        }
    }
    
    var statusColor: UIColor {
        switch status {
        //case "accepted":
            //return UIColor(red: 67.0 / 255.0, green: 160.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0)
        case "accepted", "performed":
            return UIColor(red: 30.0 / 255.0, green: 136.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
        case "canceled":
            return .appRed
        default:
            return .grayButton
        }
    }
    
    var deviceImage: UIImage? {
        switch device {
        case "mobile":
            return UIImage(named: "device_mobile")
        case "desktop":
            return UIImage(named: "device_desktop")
        case "android":
            return UIImage(named: "device_android")
        default:
            return UIImage(named: "device_ios")
        }
    }
    
}
