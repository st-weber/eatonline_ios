//
//  MSPlace+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSPlace+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSPlace (CoreDataProperties)

+ (NSFetchRequest<MSPlace *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSString *backgroundPath;
@property (nonatomic) int16_t closeHour;
@property (nullable, nonatomic, copy) NSString *coordinates;
@property (nonatomic) int16_t deliveryFrom;
@property (nonatomic) int16_t deliveryPrice;
@property (nonatomic) BOOL favourite;
@property (nullable, nonatomic, copy) NSString *footNote;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSString *kitchens;
@property (nullable, nonatomic, copy) NSString *lastUpdate;
@property (nullable, nonatomic, copy) NSString *logoPath;
@property (nonatomic) int16_t minimalOrder;
@property (nonatomic) BOOL onlinePayment;
@property (nonatomic) int16_t openHour;
@property (nonatomic) BOOL output;
@property (nonatomic) BOOL serviceDelivery;
@property (nonatomic) BOOL serviceReservation;
@property (nonatomic) BOOL serviceTakeaway;
@property (nonatomic) int16_t sort;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *type;
@property (nonatomic) BOOL updated;
@property (nullable, nonatomic, copy) NSString *workTime;
@property (nullable, nonatomic, retain) MSAction *action;
@property (nullable, nonatomic, retain) MSCity *city;
@property (nullable, nonatomic, retain) NSSet<MSDish *> *dishes;
@property (nullable, nonatomic, retain) NSSet<MSDishType *> *dishTypes;
@property (nullable, nonatomic, retain) NSSet<MSOrder *> *orders;
@property (nullable, nonatomic, retain) NSSet<MSTable *> *tables;

@end

@interface MSPlace (CoreDataGeneratedAccessors)

- (void)addDishesObject:(MSDish *)value;
- (void)removeDishesObject:(MSDish *)value;
- (void)addDishes:(NSSet<MSDish *> *)values;
- (void)removeDishes:(NSSet<MSDish *> *)values;

- (void)addDishTypesObject:(MSDishType *)value;
- (void)removeDishTypesObject:(MSDishType *)value;
- (void)addDishTypes:(NSSet<MSDishType *> *)values;
- (void)removeDishTypes:(NSSet<MSDishType *> *)values;

- (void)addOrdersObject:(MSOrder *)value;
- (void)removeOrdersObject:(MSOrder *)value;
- (void)addOrders:(NSSet<MSOrder *> *)values;
- (void)removeOrders:(NSSet<MSOrder *> *)values;

- (void)addTablesObject:(MSTable *)value;
- (void)removeTablesObject:(MSTable *)value;
- (void)addTables:(NSSet<MSTable *> *)values;
- (void)removeTables:(NSSet<MSTable *> *)values;

@end

NS_ASSUME_NONNULL_END
