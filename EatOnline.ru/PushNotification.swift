//
//  PushNotification.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 20.04.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import SwiftyJSON

class PushNotification: ResponseSerializable {

    var eventType: PushNotificationOptionType = .unknown
    var serviceContent: Any?
    
    required init?(json: JSON) {
        guard let dict = json.dictionary else { return nil }
        print(json)
        eventType = PushNotificationOptionType(rawValue: dict["type"]?.string ?? "") ?? .unknown
        switch eventType {
        case .newPlaceMessage, .placeAdvertMessage:
            if let stringId = dict["placeId"]?.string, let placeId = Int(stringId) {
                serviceContent = placeId
            }
        case .orderProcessed, .orderCanceled:
            if let stringId = dict["orderId"]?.string, let orderId = Int(stringId) {
                serviceContent = orderId
            }
        default:
            break
        }
    }
    
}
