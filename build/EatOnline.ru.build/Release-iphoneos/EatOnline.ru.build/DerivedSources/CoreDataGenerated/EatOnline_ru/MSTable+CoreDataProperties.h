//
//  MSTable+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSTable+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSTable (CoreDataProperties)

+ (NSFetchRequest<MSTable *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nonatomic) int16_t price;
@property (nonatomic) BOOL reserved;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) NSSet<MSOrder *> *orders;
@property (nullable, nonatomic, retain) MSPlace *place;

@end

@interface MSTable (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(MSOrder *)value;
- (void)removeOrdersObject:(MSOrder *)value;
- (void)addOrders:(NSSet<MSOrder *> *)values;
- (void)removeOrders:(NSSet<MSOrder *> *)values;

@end

NS_ASSUME_NONNULL_END
