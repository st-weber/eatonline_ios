//
//  PlacesViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class PlacesViewController: UniversalViewController {
    
    var dishType: DishType?
    
    override func viewDidLoad() {
        self.DS = PlacesListDS(tableInteractor: self, dishType: dishType)
        self.showSearch = true
        super.viewDidLoad()
    }
        
    override func updateData() {
        self.DS?.prepareData()
        updateTable()
    }
    
}
