//
//  AddressSuggestionsDS.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 23.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class AddressSuggestionsDS: DataSource {

    var currentAddress = ""
    var currentKladr: String?
    weak var viewController: UniversalViewController?
    var suggestions: [AddressSuggestion] = []
    
    let apiInteractor = ApiInteractor()
    var searchTimer = Timer()
    
    var becomeEmpty: ((Bool)->())?
    var addressSelected: ((AddressSuggestion)->())?
    
    init(tableInteractor: DataUpdateProtocol, viewController: UniversalViewController, currentAddress: String = "") {
        self.currentAddress = currentAddress
        self.viewController = viewController
        super.init(tableInteractor: tableInteractor)
        //NotificationCenter.subscribe(observer: self, selector: #selector(basketChanged), type: .basketChanged)

    }
    
    func addressUpdated(newAddress: String) {
        currentAddress = newAddress
        searchTimer.invalidate()
        if newAddress.count > 0 {
            searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(search), userInfo: nil, repeats: false)
        } else {
            viewController?.hideProgress()
            suggestions = []
            updateResults()
        }
    }
    
    @objc func search() {
        if currentKladr != nil {
            loadSuggestions()
        } else {
            getCurrentKladr { (kladr) in
                self.currentKladr = kladr
                self.loadSuggestions()
            }
        }
    }
    
    func getCurrentKladr(_ ready: ((String)->())?) {
        if let kladr = ConfigStorage.shared.currentCityKladr {
            ready?(kladr)
        } else {
            apiInteractor.currentKladr { (kladr, error) in
                if let error = error {
                    Alert.showError(error)
                } else if let kladr = kladr {
                    ConfigStorage.shared.currentCityKladr = kladr
                    ready?(kladr)
                }
            }
        }
    }
        
    func loadSuggestions() {
        guard currentAddress.count > 2,
              let currentKladr = currentKladr else {
            becomeEmpty?(true)
            return
        }
        viewController?.showProgress()
        apiInteractor.suggestAddress(address: currentAddress, kladr: currentKladr) { (suggestions) in
            self.viewController?.hideProgress()
            self.suggestions = suggestions
            self.updateResults()
        }
    }
    
    override func prepareData() {
        dataSource = [Section()]
    }
    
    func updateResults() {
        let section = Section()
        for suggestion in suggestions {
            //print(suggestion.value)
            let cell = Cell(simpleTitle: suggestion.value)
            cell.cellTapped = { indexPath in
                self.addressSelected?(suggestion)
            }
            section.cells.append(cell)
        }
        dataSource = [section]
        tableInteractor?.updateTable()
        becomeEmpty?(suggestions.count == 0)
    }
    
}
