//
//  Cell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

enum SeparatorInsetType {
    case none
    case full
    case standart
    case custom(CGFloat, CGFloat)
    
    var value: UIEdgeInsets {
        switch self {
        case .none:
           return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        case .full:
            return UIEdgeInsets.zero
        case .standart:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        case .custom(let left, let right):
            return UIEdgeInsets(top: 0, left: left, bottom: 0, right: right)
        }
    }
}


class Cell {

    // Properties
    var type: CellType
    var content: Any?
    var height: CGFloat
    var width: CGFloat
    var indexPath: IndexPath?
    var editingDelete: Bool = false
    var disclosureIndicator: Bool = false
    var separator: SeparatorInsetType = .full
    
    // Events
    var eventListener: CellEventProtocol?
    var cellTapped: ((IndexPath?) -> ())?
    var minusPressedEvent: ((Dish) -> ())?
    var plusPressedEvent: ((Dish) -> ())?
    var deletePressedEvent: (() -> ())?
    var boolChangedEvent: ((Bool) -> ())?
    
    
    init(cellType: CellType) {
        self.type = cellType
        self.width = cellType.width
        self.height = cellType.height
        self.eventListener = self
    }
    
    func updateSize() {
        self.width = type.width
        self.height = type.height
    }
    
}

extension Cell: CellEventProtocol {
    
    func tapEvent() {
        cellTapped?(indexPath)
    }
    
    func minusPressed(dish: Dish) {
        minusPressedEvent?(dish)
    }
    
    func plusPressed(dish: Dish) {
        plusPressedEvent?(dish)
    }
    
    func deletePressed() {
        deletePressedEvent?()
    }
    
    func boolValueChanged(_ newValue: Bool) {
        boolChangedEvent?(newValue)
    }
    
}
