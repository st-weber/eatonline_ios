//
//  MSDish+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSDish+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSDish (CoreDataProperties)

+ (NSFetchRequest<MSDish *> *)fetchRequest;

@property (nonatomic) BOOL favourite;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nonatomic) int32_t price;
@property (nonatomic) BOOL serviceDelivery;
@property (nonatomic) BOOL serviceReservation;
@property (nonatomic) BOOL serviceTakeaway;
@property (nonatomic) int16_t sort;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) int16_t weight;
@property (nullable, nonatomic, copy) NSString *weightDetails;
@property (nullable, nonatomic, copy) NSString *weightUnits;
@property (nullable, nonatomic, retain) NSSet<MSOrderAmount *> *orderAmount;
@property (nullable, nonatomic, retain) MSPlace *place;
@property (nullable, nonatomic, retain) MSDishType *type;

@end

@interface MSDish (CoreDataGeneratedAccessors)

- (void)addOrderAmountObject:(MSOrderAmount *)value;
- (void)removeOrderAmountObject:(MSOrderAmount *)value;
- (void)addOrderAmount:(NSSet<MSOrderAmount *> *)values;
- (void)removeOrderAmount:(NSSet<MSOrderAmount *> *)values;

@end

NS_ASSUME_NONNULL_END
