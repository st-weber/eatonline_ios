//
//  MSOrderAmount+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSOrderAmount+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSOrderAmount (CoreDataProperties)

+ (NSFetchRequest<MSOrderAmount *> *)fetchRequest;

@property (nonatomic) int16_t amount;
@property (nullable, nonatomic, retain) MSDish *dish;
@property (nullable, nonatomic, retain) MSOrder *order;

@end

NS_ASSUME_NONNULL_END
