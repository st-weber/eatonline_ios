//
//  BillCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class BillCell: UniversalTableViewCell {

    @IBOutlet weak var actionImage: UIImageView!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var typeImage: UIImageView!
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let action = content as? Action else { return }
        actionImage.setImage(path: action.imagePath)
        imageHeight.constant = (Screen.width - 10) * CGFloat(action.imageHeight) / CGFloat(action.imageWidth)
        typeImage.image = action.markerImage
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
}

extension Cell {
    convenience init(action: Action) {
        self.init(cellType: .billCell)
        self.content = action
    }
}
