//
//  MyOrdersViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 19.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class MyOrdersViewController: UniversalViewController, UniversalViewControllerRefreshing {
    
    let apiInteractor = ApiInteractor()
    var refreshCompletionHandler: (() -> Void)?
    var orders: [Order] = []
    var expandedMap: [Int: Bool] = [:]
    
    @IBOutlet var ordersTable: TableView!
    
    override func viewDidLoad() {
        self.showBasket = false
        self.refreshDelegate = self
        self.customTableView = ordersTable
        super.viewDidLoad()
        tableView.separatorStyle = .none
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_operator"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showOperatorActions))
        NotificationCenter.subscribe(observer: self, selector: #selector(orderUpdated), type: .orderUpdated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadOrders()
    }
    
    @objc func orderUpdated(notification: Notification) {
        /*guard let notificationContent = notification.object as? (Int, String) else { return }
        for (index, order) in orders.enumerated() {
            if order.number == notificationContent.0 {
                order.status = notificationContent.1
            }
        }*/
        refreshPulled()
    }
    
    func loadOrders() {
        if orders.count == 0 {
            showProgress()
        }
        apiInteractor.loadMyOrders { (orders) in
            if self.orders.count == 0 {
                self.hideProgress()
            } else {
                self.refreshCompletionHandler?()
            }
            self.drawOrders(orders)
        }
    }
    
    func refreshPulled() {
        loadOrders()
    }
    
    func drawOrders(_ orders: [Order]) {
        self.orders = orders
        var sections: [Section] = []
        for (index, order) in orders.enumerated() {
            expandedMap[index] = false
            let section = Section()
            section.cells.append(getOrderCell(order, expanded: false))
            sections.append(section)
        }
        if sections.count > 0 {
            dataSource = sections
        } else {
            dataSource = [Section()]
        }
        updateTable()
    }
    
    func getOrderCell(_ order: Order, expanded: Bool) -> Cell {
        let cell = Cell(order: order, expanded: expanded)
        cell.cellTapped = { indexPath in
            self.orderPressed(order.number)
        }
        return cell
    }
    
    func orderPressed(_ orderNumber: Int32) {
        for (index, order) in orders.enumerated() {
            if order.number == orderNumber, let expanded = expandedMap[index] {
                expandOrder(expand: !expanded, sectionIndex: index)
                return
            }
        }
    }

    func expandOrder(expand: Bool, sectionIndex: Int) {
        var paths: [IndexPath] = []
        if expand {
            let detailsCell = Cell(orderDetails: orders[sectionIndex])
            dataSource[sectionIndex].cells.append(detailsCell)
            paths.append(IndexPath(row: 1, section: sectionIndex))
            
            for (itemIndex, item) in orders[sectionIndex].items.enumerated() {
                dataSource[sectionIndex].cells.append(Cell(orderItem: item))
                paths.append(IndexPath(row: itemIndex + 2, section: sectionIndex))
            }
            insertRows(indexPaths: paths, animation: .top)
            
        } else {
            for rowIndex in (1..<dataSource[sectionIndex].cells.count).reversed() {
                paths.append(IndexPath(row: rowIndex, section: sectionIndex))
                //dataSource[sectionIndex].cells.dropLast()
                dataSource[sectionIndex].cells.remove(at: rowIndex)
            }
            deleteRows(indexPaths: paths, animation: .bottom)
        }
        expandedMap[sectionIndex] = expand
        dataSource[sectionIndex].cells[0] = getOrderCell(orders[sectionIndex], expanded: expand)
        updateRows(indexPaths: [IndexPath(row: 0, section: sectionIndex)], animation: .fade)
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
