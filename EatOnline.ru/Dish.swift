//
//  Dish.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 07.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import Foundation

extension Dish {
    
    func getWeightText(amount: Int = 1) -> String? {
        return "\(Int(weight) * amount) \(weightUnits ?? "гр.")"
    }
    
    func getPrice(amount: Int = 1) -> String? {
        return "\(Int(price) * Int(amount)) р."
    }
    
    func switchFavourite() {
        favourite = !favourite
        CoreDataManager.instance.saveContext()
        NotificationCenter.post(type: .favouriteDishSwitched, object: self)
    }
    
}
