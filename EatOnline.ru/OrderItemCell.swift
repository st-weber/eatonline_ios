//
//  OrderItemCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 20.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class OrderItemCell: UniversalTableViewCell {

    @IBOutlet var itemLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        guard let orderAmount = content as? OrderAmount else { return }
        itemLabel.text = orderAmount.dish?.name
        priceLabel.text = "\(orderAmount.dish?.price ?? 0) р."
        amountLabel.text = "x \(orderAmount.amount)"
    }
    
}

extension Cell {
    convenience init(orderItem: OrderAmount) {
        self.init(cellType: .orderItem)
        self.content = orderItem
    }
}
