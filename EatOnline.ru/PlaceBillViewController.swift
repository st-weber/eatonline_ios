//
//  PlaceBillViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 18.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class PlaceBillViewController: UniversalViewController {

    let placeId: Int
    
    @IBOutlet var billTable: TableView!
    
    init(placeId: Int) {
        self.placeId = placeId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.customTableView = billTable
        super.viewDidLoad()
    }
    
    override func prepareData() {
        let actions = CoreDataManager.instance.getActions(placeId: placeId)
        if actions.count > 0 {
            let section = Section()
            for action in actions {
                let actionCell = Cell(action: action)
                section.cells.append(actionCell)
            }
            dataSource = [section]
            updateTable()
            billTable.isHidden = false
        } else {
            billTable.isHidden = true
        }        
    }

}
