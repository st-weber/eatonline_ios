//
//  MSOrder+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSOrder+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSOrder (CoreDataProperties)

+ (NSFetchRequest<MSOrder *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *date;
@property (nonatomic) int16_t finalPrice;
@property (nonatomic) int16_t number;
@property (nonatomic) int16_t status;
@property (nonatomic) int16_t type;
@property (nullable, nonatomic, retain) NSSet<MSOrderAmount *> *amounts;
@property (nullable, nonatomic, retain) MSPlace *place;
@property (nullable, nonatomic, retain) MSTable *table;

@end

@interface MSOrder (CoreDataGeneratedAccessors)

- (void)addAmountsObject:(MSOrderAmount *)value;
- (void)removeAmountsObject:(MSOrderAmount *)value;
- (void)addAmounts:(NSSet<MSOrderAmount *> *)values;
- (void)removeAmounts:(NSSet<MSOrderAmount *> *)values;

@end

NS_ASSUME_NONNULL_END
