//
//  LoadingCoordinator.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 24.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class LoadingCoordinator {
    
    static let shared = LoadingCoordinator()
    
    let apiInteractor = ApiInteractor()
    var isLoading = false
    var isLoadingMenu = false
    var loadedPlacesCount = 0
    
    func loadData() {
        guard let currentCityId = ConfigStorage.shared.currentCityId else {
            Router.shared.startApp()
            return
        }

        isLoading = true
        NotificationCenter.post(type: .startLoadData)
        
        apiInteractor.loadSettings {
            self.apiInteractor.loadDishTypes {
                self.apiInteractor.loadPlacesList(currentCity: currentCityId) {
                    self.apiInteractor.loadActions(currentCity: currentCityId) {
                        self.isLoading = false
                        NotificationCenter.post(type: .dataLoaded)
                        self.checkPlacesMenu()
                    }
                }
            }
        }
    }
    
    private func checkPlacesMenu() {
        guard let currentCityId = ConfigStorage.shared.currentCityId else { return }
        let notUpdatedPlaces = CoreDataManager.instance.getPlaces(currentCityId: currentCityId, needToUpdate: true)
        if notUpdatedPlaces.count > 0 {
            updatePlacesMenu(notUpdatedPlaces)
        } else {
            NotificationCenter.post(type: .placesMenuLoaded)
        }
    }
    
    private func updatePlacesMenu(_ placesList: [Place]) {
        loadedPlacesCount = 0
        isLoadingMenu = true
        NotificationCenter.post(type: .startMenuLoading)
        
        //DispatchQueue.global(qos: .background).async {
            for place in placesList {
                //print("\(place.name ?? "") need to update")
                DispatchQueue.global(qos: .background).async {
                    self.apiInteractor.loadPlaceDishes(placeId: Int(place.id)) {
                        self.loadedPlacesCount += 1
                        if self.loadedPlacesCount == placesList.count {
                            self.isLoadingMenu = false
                            DispatchQueue.main.async {
                                NotificationCenter.post(type: .placesMenuLoaded)
                            }
                        }
                        DispatchQueue.main.async {
                            if let updatedPlace = CoreDataManager.instance.getPlaceById(Int(place.id)) {
                                //print("\(updatedPlace.name ?? "") updated dishes: \(updatedPlace.dishes?.count ?? 0)")
                                NotificationCenter.post(type: .placeMenuLoaded, object: updatedPlace)
                            }
                        }
                    }
                }
            }
        //}
    }

}
