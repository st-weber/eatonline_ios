//
//  CoreDataManager+DishTypes.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 31/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import CoreData
import SwiftyJSON

extension CoreDataManager {

    func getDishTypeById(_ id: Int) -> DishType? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DishType")
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let type = result.first as? DishType {
                return type
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func getPlaceDishTypes(_ placeId: Int) -> [DishType] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DishType")
        //request.predicate = NSPredicate(format: "ANY places.id == %@ AND type == NULL", "\(placeId)")
       
        request.predicate = NSPredicate(format: "ANY places.id == %@ AND type == NULL", "\(placeId)")
        
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let types = result as? [DishType] {
                return types
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func getDishTypes(forMainScreen: Bool = false, context: NSManagedObjectContext? = nil) -> [DishType] {
        var currentContext = persistentContainer.viewContext
        if let context = context {
            currentContext = context
        }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DishType")
        if forMainScreen {
             request.predicate = NSPredicate(format: "imagePath != nil AND places.@count > 0")
            //imagePath != NULL AND category == %@ AND places.@count > 0
        }
        
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try currentContext.fetch(request)
            if let types = result as? [DishType] {
                return types
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func getTypeSubtypes(dishTypeId: Int, placeId: Int) -> [DishType] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "DishType")
        request.predicate = NSPredicate(format: "ANY places.id == %@ AND type.id = %@", "\(placeId)", "\(dishTypeId)")
        
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let types = result as? [DishType] {
                return types
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func createOrUpdateDishTypes(json: [JSON]) {
        var oldTypes: [Int32: DishType] = [:]
        for type in getDishTypes() {
            oldTypes[type.id] = type
        }
        
        for typeJson in json {
            if let type = createOrUpdateDishType(json: typeJson) {
                oldTypes.removeValue(forKey: type.id)
                saveContext()
            }
        }
        
        for oldType in oldTypes.values {
            print("delete oldType: \(oldType.name!)")
            persistentContainer.viewContext.delete(oldType)
        }
        saveContext()
    }
    
    private func createOrUpdateDishType(json: JSON) -> DishType? {
        guard let dict = json.dictionary,
            let id = dict["id"]?.int,
            let name = dict["name"]?.string else { return nil }
    
        var type: DishType?
        if let coreType = getDishTypeById(id) {
            type = coreType
        } else {
            type = DishType.init(context: persistentContainer.viewContext)
            type?.id = Int32(id)
        }
        
        type?.name = name
        type?.sort = Int16(dict["sort"]?.int ?? 0)
        type?.iconPath = dict["icon"]?.string
        type?.imagePath = dict["image"]?.string
        
        if let parentId = dict["parent"]?.int, let parentType = getDishTypeById(parentId) {
            type?.type = parentType
        }
        
        return type
    }
    
}
