//
//  Basket.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 08.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class Basket {

    static let current = Basket()
    
    var currentAddress: String?
    
    var currentOrder: Order {
        return CoreDataManager.instance.getCurrentOrder()
    }
    
    func clear() {
        currentOrder.place = nil
        for amount in currentOrder.items {
            CoreDataManager.instance.deleteAmount(amount)
        }
        CoreDataManager.instance.saveContext()
        //currentOrder.save()
        NotificationCenter.post(type: .basketCleared)
    }
    
    func incrementDish(_ dish: Dish) {
        guard let dishPlace = dish.place else { return }
        if let closedText = dishPlace.isClosed {
            Alert.show(title: "Заведение закрыто", message: closedText, actions: nil)
            return
        }
        
        if let currentPlace = currentOrder.place {
            if currentPlace.id == dishPlace.id {
                incrementDishAmount(dish)
                return
            } else {
                showClearBasket(yes: {
                    self.clear()
                    NotificationCenter.post(type: .basketChanged)
                    self.incrementDish(dish)
                })
                return
            }
        } else {
            currentOrder.place = dish.place
            incrementDishAmount(dish)
            return
        }
    }
    
    private func incrementDishAmount(_ dish: Dish) {
        for amount in currentOrder.items {
            guard let amountDish = amount.dish else { continue }
            if amountDish.id == dish.id {
                amount.amount += 1
                CoreDataManager.instance.saveContext()
                NotificationCenter.post(type: .basketChanged, object: amount)
                return
            }
        }
        let newAmount = CoreDataManager.instance.createAmount(dish: dish, order: Basket.current.currentOrder)
        NotificationCenter.post(type: .basketChanged, object: newAmount)
    }
    
    
    func decrementDish(_ dish: Dish, nonZero: Bool = false) {
        for amount in currentOrder.items {
            guard let amountDish = amount.dish else { continue }
            if amountDish.id == dish.id {
                if nonZero, amount.amount <= 1 {
                    return
                }
                amount.amount -= 1
                if amount.amount <= 0 {
                    deleteAmount(amount)
                } else {
                    CoreDataManager.instance.saveContext()
                    NotificationCenter.post(type: .basketChanged, object: amount)
                }
                return
            }
        }
        
    }
    
    func deleteAmount(_ amount: OrderAmount) {
        let dish = amount.dish
        CoreDataManager.instance.deleteAmount(amount)
        NotificationCenter.post(type: .basketChanged, object: dish)
        if currentOrder.items.count == 0 {
            clear()
        }
    }
        
    private func showClearBasket(yes: (()->())?) {
        let yesAction = (title: "Да", callback: { () -> Void in
            yes?()
        })
        let noAction = (title: "Нет", callback: { () -> Void in })
        Alert.show(title: "Очистить корзину?", message: "Ранее добавленные блюда из другого заведения будут удалены", actions: [yesAction, noAction])
    }
    
}
