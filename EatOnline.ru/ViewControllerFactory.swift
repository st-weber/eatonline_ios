//
//  ViewControllerFactory.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

enum ViewControllerFactory {

    case start
    case tabBarController
    
    case bill
    case places
    case dishTypePlaces(DishType)
    case dishes
    case favourites
    case search(((Any)->())?)
    
    case profile
    case profilePhone
    case profileCity
    case wroteUs
    case myOrders
    
    case place(Place, DishType?, Dish?)
    case disheDetails([Dish], Int)
    case basket
    case checkout
    case addressPicker(Place, String?)
    case checkPhone(String, Bool)
    case orderCreated(Int)
    
    case textPage(String, String)
    
    var create: UIViewController {
        switch self {
        case .start:
            return StartViewController()
        
        case .tabBarController:
            return TabBarViewController(currentPage: 1)
            
        case .bill:
            let vc = BillViewController()
            vc.title = "Афиша"
            vc.tabBarItem = UITabBarItem(title: "Афиша", image: UIImage(named: "tab_bill"), selectedImage: nil)
            return vc
            
        case .places:
            let vc = PlacesViewController()
            vc.title = "Заведения"
            vc.tabBarItem = UITabBarItem(title: "Заведения", image: UIImage(named: "tab_places"), selectedImage: nil)
            return vc
            
        case .dishTypePlaces(let dishType):
            let vc = PlacesViewController()
            vc.dishType = dishType
            vc.title = dishType.name
            return vc
            
        case .dishes:
            let vc = DishTypesViewController()
            vc.title = "Блюда"
            vc.tabBarItem = UITabBarItem(title: "Блюда", image: UIImage(named: "tab_dishes"), selectedImage: nil)
            return vc
            
        case .favourites:
            let placesVC = FavouritePlacesViewController()
            placesVC.title = "Заведения"
            
            let dishesVC = FavouriteDishesViewController()
            dishesVC.title = "Блюда"
            
            let pagerVC = PagerViewController(viewControllers: [placesVC, dishesVC], mode: .top)
            pagerVC.title = "Избранное"
            pagerVC.tabBarItem = UITabBarItem(title: "Избранное", image: UIImage(named: "tab_favourites"), selectedImage: nil)
            return pagerVC

        case .search(let callback):
            let vc = SearchViewController()
            vc.title = "Поиск"
            vc.callback = callback
            return vc
            
        case .profile:
            let vc = ProfileViewController()
            vc.title = "Профиль"
            vc.tabBarItem = UITabBarItem(title: "Профиль", image: UIImage(named: "tab_profile"), selectedImage: nil)
            return vc
        case .profilePhone:
            let vc = EnterPhoneViewController()
            vc.title = "Войти"
            return vc
            
        case .profileCity:
            let vc = CitySelectViewController()
            vc.title = "Выбор города"
            return vc
        case .wroteUs:
            let vc = WroteUsViewController()
            vc.title = "Написать нам"
            return vc
        case .myOrders:
            let vc = MyOrdersViewController()
            vc.title = "Мои заказы"
            return vc
            
        case .place(let place, let dishType, let dish):
            let placeVC = PlaceViewController(place: place)
            placeVC.startType = dishType
            placeVC.startDish = dish
            placeVC.title = "Меню"
            
            let billVC = PlaceBillViewController(placeId: Int(place.id))
            billVC.title = "Афиша"
            
            //let reviewsVC = PlaceReviewsViewController(placeId: Int(place.id))
            //reviewsVC.title = "Отзывы"
            
            //var viewControllers =
            /*if CoreDataManager.instance.getActions(placeId: Int(place.id)).count > 0 {
                let billVC = BillViewController()
                billVC.placeId = Int(place.id)
                billVC.title = "Афиша"
                viewControllers.append(billVC)
            }*/
            let pagerVC = PagerViewController(viewControllers: [placeVC, billVC/*, reviewsVC*/], mode: .top)
            pagerVC.title = place.name
            return pagerVC
            
        case .disheDetails(let dishes, let current):
            var viewControllers: [UniversalViewController] = []
            for dish in dishes {
                viewControllers.append(DishDetailsViewController(dish: dish))
            }
            let pagerVC = PagerViewController(viewControllers: viewControllers, currentPage: current, mode: .bottom)
            pagerVC.showBasket = false
            pagerVC.addCancelButton()
            pagerVC.title = dishes.first?.type?.name
            return pagerVC
            
        case .basket:
            let vc = BasketViewController()
            if let placeName = Basket.current.currentOrder.place?.name {
                vc.title = "\(placeName) - корзина"
            } else {
                vc.title = "Корзина"
            }
            
            return vc
        case .checkout:
            let vc =  CheckoutViewController()
            vc.title = "Оформить заказ"
            return vc
        case .addressPicker(let place, let currentAddress):
            let vc = AddressPickerViewController(place: place)
            vc.title = "Адрес доставки"
            vc.currentAddress = currentAddress
            return vc
        case .checkPhone(let phone, let authorisationMode):
            let vc = CheckPhoneViewController(phone: phone, authorisationMode: authorisationMode)
            vc.title = "Подтверждение"
            return vc
        case .orderCreated(let orderNumber):
            let vc = OrderCreatedViewController(orderNumber: orderNumber)
            return vc
            
        case .textPage(let pageName, let endpoint):
            let textVC = TextViewController(endpoint: endpoint)
            textVC.title = pageName
            return textVC
        }
    }
    
}
