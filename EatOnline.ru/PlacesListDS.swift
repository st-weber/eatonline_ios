//
//  PlacesListDS.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 06/11/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class PlacesListDS: DataSource {

    let dishType: DishType?
    let favourites: Bool
    
    var placesMap: [Int32:Int] = [:]
    var becomeEmpty: ((Bool)->())?
    
    init(tableInteractor: DataUpdateProtocol, dishType: DishType? = nil, favourites: Bool = false) {
        self.dishType = dishType
        self.favourites = favourites
        super.init(tableInteractor: tableInteractor)
        
        NotificationCenter.subscribe(observer: self, selector: #selector(placeUpdated), type: .favouritePlaceSwitched)
        NotificationCenter.subscribe(observer: self, selector: #selector(placeMenuLoaded), type: .placeMenuLoaded)
        NotificationCenter.subscribe(observer: self, selector: #selector(reloadData), type: .placesMenuLoaded)
    }
    
    override func prepareData() {
        guard let currentCityId = ConfigStorage.shared.currentCityId else { return }
        let section = Section()
        placesMap = [:]
        
        let placesList = CoreDataManager.instance.getPlaces(currentCityId: currentCityId, dishType: dishType, favourites: favourites)
        for (index, place) in placesList.enumerated() {
            let placeCell = Cell(placeList: place)
            placeCell.cellTapped = { indexPath in
                
                if !place.needToUpdate {
                    Router.shared.pushController(.place(place, self.dishType, nil))
                }
            }
            section.cells.append(placeCell)
            placesMap[place.id] = index
        }
        dataSource = [section]
        becomeEmpty?(dataSource[0].cells.count == 0)
    }
    
    @objc func reloadData() {
        prepareData()
    }
    
    @objc func placeUpdated(notification: Notification) {
        guard let place = notification.object as? Place else { return }
        if favourites, !place.favourite, let index = placesMap[place.id] {
            let indexPath = IndexPath(row: index, section: 0)
            dataSource[0].cells.remove(at: index)
            tableInteractor?.deleteRows(indexPaths: [indexPath], animation: .automatic)
            placesMap.removeValue(forKey: place.id)
            for placePos in placesMap {
                if placePos.value > index {
                    placesMap[placePos.key] = placePos.value - 1
                }
            }
        } else if let index = placesMap[place.id] {
            let indexPath = IndexPath(row: index, section: 0)
            tableInteractor?.updateCellContent(indexPath: indexPath)
        } else {
            prepareData()
            tableInteractor?.updateTable()
        }
        if dataSource[0].cells.count == 1 {
            becomeEmpty?(false)
        } else if dataSource[0].cells.count == 0 {
            becomeEmpty?(true)
        }
    }
    
    @objc func placeMenuLoaded(notification: Notification) {
        guard let place = notification.object as? Place,
              let index = placesMap[place.id] else { return }
        //DispatchQueue.main.async {
            //print((place.name ?? "") + " needToUpdate: \(place.needToUpdate)")
            self.tableInteractor?.updateCellContent(indexPath: IndexPath(row: index, section: 0))
        //}
    }
    
    deinit {
        NotificationCenter.unsubscribe(self)
    }
    
}
