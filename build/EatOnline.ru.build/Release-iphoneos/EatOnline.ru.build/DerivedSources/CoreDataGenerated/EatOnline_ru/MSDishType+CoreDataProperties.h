//
//  MSDishType+CoreDataProperties.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSDishType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MSDishType (CoreDataProperties)

+ (NSFetchRequest<MSDishType *> *)fetchRequest;

@property (nonatomic) int16_t category;
@property (nullable, nonatomic, retain) NSData *iconData;
@property (nullable, nonatomic, copy) NSString *iconPath;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nonatomic) int16_t sort;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) NSSet<MSDish *> *dishes;
@property (nullable, nonatomic, retain) NSSet<MSPlace *> *places;
@property (nullable, nonatomic, retain) NSSet<MSDishType *> *subtypes;
@property (nullable, nonatomic, retain) MSDishType *type;

@end

@interface MSDishType (CoreDataGeneratedAccessors)

- (void)addDishesObject:(MSDish *)value;
- (void)removeDishesObject:(MSDish *)value;
- (void)addDishes:(NSSet<MSDish *> *)values;
- (void)removeDishes:(NSSet<MSDish *> *)values;

- (void)addPlacesObject:(MSPlace *)value;
- (void)removePlacesObject:(MSPlace *)value;
- (void)addPlaces:(NSSet<MSPlace *> *)values;
- (void)removePlaces:(NSSet<MSPlace *> *)values;

- (void)addSubtypesObject:(MSDishType *)value;
- (void)removeSubtypesObject:(MSDishType *)value;
- (void)addSubtypes:(NSSet<MSDishType *> *)values;
- (void)removeSubtypes:(NSSet<MSDishType *> *)values;

@end

NS_ASSUME_NONNULL_END
