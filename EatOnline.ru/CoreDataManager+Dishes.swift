//
//  CoreDataManager+Dishes.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 06/11/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import CoreData
import SwiftyJSON

extension CoreDataManager {

    func getDishById(_ id: Int, context: NSManagedObjectContext? = nil) -> Dish? {
        var currentContext = persistentContainer.viewContext
        if let context = context {
            currentContext = context
        }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dish")
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        do {
            let result = try currentContext.fetch(request)
            if let dish = result.first as? Dish {
                return dish
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func getDishes(placeId: Int, context: NSManagedObjectContext? = nil) -> [Dish] {
        var currentContext = persistentContainer.viewContext
        if let context = context {
            currentContext = context
        }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dish")
        request.predicate = NSPredicate(format: "place.id = %@", "\(placeId)")
        //let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        //request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try currentContext.fetch(request)
            if let dishes = result as? [Dish] {
                return dishes
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func getFavouriteDishes() -> [Dish] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dish")
        request.predicate = NSPredicate(format: "favourite == YES")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let dishes = result as? [Dish] {
                return dishes
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func getTypeDishes(placeId: Int, dishTypeId: Int) -> [Dish] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dish")
        request.predicate = NSPredicate(format: "place.id = %@ AND type.id = %@", "\(placeId)", "\(dishTypeId)")
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        let sortPriceDescriptor = NSSortDescriptor(key: "price", ascending: true)
        request.sortDescriptors = [sortDescriptor, sortPriceDescriptor]
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let dishes = result as? [Dish] {
                return dishes
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func searchDishesByName(_ searchText: String) -> [Dish] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dish")
        request.predicate = NSPredicate(format: "name contains[cd] %@", searchText)
        let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let dishes = result as? [Dish] {
                return dishes
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func createOrUpdateDishesList(placeId: Int, json: [JSON], created: (()->())?) {
        let asyncContext = self.persistentContainer.newBackgroundContext()
        asyncContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        asyncContext.undoManager = nil
        
        guard let currentPlace = self.getPlaceById(placeId, context: asyncContext) else { return }
        
        var dishTypes: [Int: DishType] = [:]
        for type in self.getDishTypes(context: asyncContext) {
            dishTypes[Int(type.id)] = type
        }
        
        var oldDishes: [Int: Dish] = [:]
        for dish in self.getDishes(placeId: placeId, context: asyncContext) {
            oldDishes[Int(dish.id)] = dish
        }
        
        for dishJson in json {
            let newDishInfo = self.createOrUpdateDish(place: currentPlace, json: dishJson, context: asyncContext)
            if let dish = newDishInfo.0, let dishType = dishTypes[newDishInfo.1] {
                dish.type = dishType
                dishType.addToPlaces(currentPlace)
                if let parentType = dishType.type {
                    parentType.addToPlaces(currentPlace)
                }
                oldDishes.removeValue(forKey: Int(dish.id))
            }
        }
        
        for oldDish in oldDishes.values {
            print("delete oldDish: \(oldDish.name!)")
            /*if let dishType = oldDish.type {
                dishType.removeFromPlaces(currentPlace)
                if let parentType = dishType.type {
                    parentType.removeFromPlaces(currentPlace)
                }
            }*/
            asyncContext.delete(oldDish)
        }
        currentPlace.needToUpdate = false
        self.saveOtherContext(asyncContext) {
            created?()
        }
    }
    
    private func createOrUpdateDish(place: Place, json: JSON, context: NSManagedObjectContext) -> (Dish?, Int) {
        guard let dict = json.dictionary,
            let id = dict["id"]?.int,
            let typeId = dict["type"]?.int,
            let name = dict["name"]?.string else { return (nil, 0) }
    
        var dish: Dish?
        if let coreDish = getDishById(id, context: context) {
            dish = coreDish
        } else {
            dish = Dish.init(context: context)
            dish?.id = Int32(id)
        }
        
        dish?.name = name.htmlDecoded
        dish?.imagePath = dict["img"]?.string
        dish?.bigImagePath = dict["bimg"]?.string
        dish?.price = Int32(dict["price"]?.int ?? 0)
        dish?.weight = Int16(dict["weight"]?.int ?? 0)
        dish?.sort = Int16(dict["sort"]?.int ?? 0)
        dish?.weightUnits = dict["units"]?.string
        dish?.details = dict["descr"]?.string?.htmlDecoded
        dish?.place = place
            
        return (dish, typeId)
    }
    
}
