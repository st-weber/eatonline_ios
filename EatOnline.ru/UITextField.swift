//
//  UITextField.swift
//  Direct Farm
//
//  Created by Michail Solyanic on 13.04.2020.
//  Copyright © 2020 Direct.Farm. All rights reserved.
//

import UIKit

extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
