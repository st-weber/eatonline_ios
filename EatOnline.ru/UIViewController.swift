//
//  UIViewController.swift
//  vmeste
//
//  Created by Michail Solyanic on 19.11.2019.
//  Copyright © 2019 Direct.Farm. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func addCancelButton() {
        let backButton = UIBarButtonItem(image: UIImage(named: "nav_close"), style: .plain, target: self, action: #selector(dismissController))
        //let backButton = UIBarButtonItem(title: "cancel_title".localized, style: .plain, target: self, action: #selector(dismissController))
        navigationItem.leftBarButtonItem = backButton
    }
    
    func addBackButton() {
        let backButton = UIBarButtonItem(image: UIImage(named: "nav_back"), style: .plain, target: self, action: #selector(goBack))
        //let backButton = UIBarButtonItem(title: "back_title".localized, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
        if let navigationController = navigationController as? NavigationController {
            navigationController.goBackPressed?()
        }
    }
    
}
