//
//  MSTable+CoreDataProperties.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSTable+CoreDataProperties.h"

@implementation MSTable (CoreDataProperties)

+ (NSFetchRequest<MSTable *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MSTable"];
}

@dynamic identifier;
@dynamic imagePath;
@dynamic price;
@dynamic reserved;
@dynamic title;
@dynamic orders;
@dynamic place;

@end
