//
//  ApiInteractor.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/09/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit
import SwiftyJSON

class ApiInteractor: NSObject {

    func loadCities(completed: (()->())?) {
        NetworkService.shared.getJsonData(endpoint: .locations) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadCities(completed: completed)
                }
            } else if let array = json?.array {
                CoreDataManager.instance.createOrUpdateLocations(json: array)
                completed?()
            }
        }
    }
    
    @objc func loadSettings(completed: (()->())?) {
        NetworkService.shared.getJsonData(endpoint: .configSettings) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadSettings(completed: completed)
                }
            } else {
                ConfigStorage.shared.operatorPhone = json?.dictionary?["operator"]?.string
                ConfigStorage.shared.ownerPhone = json?.dictionary?["owner"]?.string
                ConfigStorage.shared.hotlinePhone = json?.dictionary?["hotline"]?.string
                if let array = json?.dictionary?["info_pages"]?.array {
                    ConfigStorage.shared.infoPages = []
                    for json in array {
                        if let page = InfoPage(json: json) {
                            ConfigStorage.shared.infoPages.append(page)
                        }
                    }
                }
                UserDefaults.standard.synchronize()
                completed?()
            }
        }
    }
    
    func loadDishTypes(completed: (()->())?) {
        NetworkService.shared.getJsonData(endpoint: .dishTypes) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadDishTypes(completed: completed)
                }
            } else if let array = json?.array {
                CoreDataManager.instance.createOrUpdateDishTypes(json: array)
                completed?()
            }
        }
    }
    
    func loadPlacesList(currentCity: Int, completed: (()->())?) {
        NetworkService.shared.getJsonData(endpoint: .placesList(currentCity)) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadPlacesList(currentCity: currentCity, completed: completed)
                }
            } else if let array = json?.array {
                CoreDataManager.instance.createOrUpdatePlacesList(currentCityId: currentCity, json: array)
                completed?()
            }
        }
    }
    
    func loadPlaceDishes(placeId: Int, completed: (()->())?) {
        NetworkService.shared.getJsonData(endpoint: .placeDishes(placeId), assync: true) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadPlaceDishes(placeId: placeId, completed: completed)
                }
            } else if let array = json?.array {
                CoreDataManager.instance.createOrUpdateDishesList(placeId: placeId, json: array, created: completed)
            }
        }
    }
    
    func loadActions(currentCity: Int, completed: (()->())?) {
        NetworkService.shared.getJsonData(endpoint: .actions(currentCity)) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadActions(currentCity: currentCity, completed: completed)
                }
            } else if let array = json?.array {
                CoreDataManager.instance.createOrUpdateActions(json: array)
                completed?()
            }
        }
    }
    
    func loadTextPage(page: Endpoint, completed: ((String)->())?) {
        NetworkService.shared.getJsonData(endpoint: page) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.loadTextPage(page: page, completed: completed)
                }
            } else if let text = json?.dictionary?["text"]?.string {
                completed?(text)
            } else {
                completed?("")
            }
            
        }
    }
    
    func sendWrotaUsLetter(contact: String, text: String, completed: ((Error?)->())?) {
        let parameters = [
            "contact": contact,
            "text": text,
            "device": "iOS"
            ] as [String: AnyObject]
        
        NetworkService.shared.getJsonData(endpoint: .wroteUs, parameters: parameters) { (json, error) in
            if let error = error {
                completed?(error)
            } else if let result = json?.dictionary?["result"]?.string, result == "success" {
                completed?(nil)
            } else {
                completed?(NSError.sendingError)
            }
            
        }
    }
    
    func currentKladr(completed: ((String?, Error?)->())?) {
        guard let currentCityId = ConfigStorage.shared.currentCityId,
            let cityName = CoreDataManager.instance.getCityById(currentCityId)?.name else { return }
        NetworkService.shared.getJsonData(endpoint: .kladrSuggest(cityName)) { (json, error) in
            if let error = error {
                completed?(nil, error)
            } else if let result = json?.dictionary?["result"]?.string, result != "failed" {
                completed?(result, nil)
            } else {
                completed?(nil, NSError.sendingError)
            }
        }
    }
    
    func suggestAddress(address: String, kladr: String, completed: (([AddressSuggestion])->())?) {
        let parameters = ["kladr": kladr] as [String: AnyObject]
        NetworkService.shared.getJsonData(endpoint: .addressSuggest(address), parameters: parameters) { (json, error) in
            var suggestions: [AddressSuggestion] = []
            if let error = error {
                Alert.showError(error)
            } else if let suggestionsArr = json?.dictionary?["suggestions"]?.array {
                for suggestionJson in suggestionsArr {
                    if let suggestion = AddressSuggestion(json: suggestionJson) {
                        suggestions.append(suggestion)
                    }
                }
            }
            completed?(suggestions)
        }
    }
    
    func currentAddress(lat: String, lon: String, completed: (([AddressSuggestion])->())?) {
        NetworkService.shared.getJsonData(endpoint: .addressDetect(lat, lon)) { (json, error) in
            var suggestions: [AddressSuggestion] = []
            if let error = error {
                Alert.showError(error)
            } else if let suggestionsArr = json?.dictionary?["suggestions"]?.array {
                for suggestionJson in suggestionsArr {
                    if let suggestion = AddressSuggestion(json: suggestionJson) {
                        suggestions.append(suggestion)
                    }
                }
            }
            completed?(suggestions)
        }
    }
    
    func requestCode(phone: String, completed: ((Int?, Int?, Error?)->())?) {
        let parameters = ["phone": phone] as [String: AnyObject]
        NetworkService.shared.getJsonData(endpoint: .requestCode, parameters: parameters) { (json, error) in
            completed?(json?.dictionary?["resend_delay"]?.int, json?.dictionary?["resend_current"]?.int, error)
        }
    }
    
    func checkCode(phone: String, code: String, completed: ((Bool)->())?) {
        let parameters = ["phone": phone, "code": code] as [String: AnyObject]
        NetworkService.shared.getJsonData(endpoint: .checkCode, parameters: parameters) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    completed?(false)
                }
            } else if let hash = json?.dictionary?["hash"]?.string {
                ConfigStorage.shared.authHash = hash
                ConfigStorage.shared.loginPhone = phone
                NotificationCenter.post(type: .userAuthorised)
                completed?(true)
            } else {
                completed?(false)
            }
        }
    }
    
    func createOrder(completed: ((Int?)->())?) {
        let order = Basket.current.currentOrder
        
        guard let placeId = order.place?.id else {
            Alert.showErrorMessage("Не известное заведение")
            return
        }
        
        var amounts: [String: Int] = [:]
        for amount in order.items {
            if let dishId = amount.dish?.id {
                amounts[String(dishId)] = Int(amount.amount)
            }
        }
        let parameters: [String : Any] = [
            "name": ConfigStorage.shared.myName ?? "Клиент",
            "type": Int(order.type),
            "place_id": placeId,
            "address": order.address ?? "",
            "device": "ios",
            "persons": Int(order.persons),
            "for_time": order.forTime ?? "",
            "change_from": order.changeFrom ?? "",
            "payment_type": order.paymentType ?? "",
            "commets": order.comments ?? "",
            "amounts": amounts,
            "firebase_token": ConfigStorage.shared.firebaseToken ?? ""
            ]
        
        //print(parameters)
        NetworkService.shared.getJsonData(endpoint: .createOrder, parameters: parameters as [String : AnyObject]) { (json, error) in
            if let error = error {
                Alert.showError(error) {
                    self.createOrder(completed: completed)
                }
            } else if let orderNumber = json?.dictionary?["order"]?.int {
                completed?(orderNumber)
            } else {
                Alert.showErrorMessage("Что то пошло не так")
                completed?(nil)
            }
        }
    }
    
    func logIn(completed: (()->())?) {
        guard let phone = ConfigStorage.shared.loginPhone,
            let authHash = ConfigStorage.shared.authHash else { completed?(); return }
        let parameters = ["phone": phone, "hash": authHash] as [String: AnyObject]
        NetworkService.shared.getJsonData(endpoint: .silentLogin, parameters: parameters) { (json, error) in
            if let error = error {
                Alert.showError(error)
            } else if (json?.dictionary?["error"]?.string) != nil {
                NotificationCenter.post(type: .userUnauthorised)
            }
            completed?()
        }
    }
    
    func loadMyOrders(completed: (([Order])->())?) {
        NetworkService.shared.getJsonData(endpoint: .myOrders) { (json, error) in
            var orders: [Order] = []
            if let error = error {
                Alert.showError(error)
            } else if let array = json?.array {
                orders = CoreDataManager.instance.createOrUpdateOrdersList(json: array) 
            }
            completed?(orders)
        }
    }
    
    func pushSubscribe(completed: ((Error?)->())?) {
        guard let currentCityId = ConfigStorage.shared.currentCityId,
            let token = ConfigStorage.shared.firebaseToken else {
            completed?(nil)
            return
        }
        let parameters = ["device": "ios",
                          "location": currentCityId,
                          "token": token] as [String: AnyObject]
        NetworkService.shared.getJsonData(endpoint: .pushSubscribe, parameters: parameters, assync: true) { (json, error) in
            DispatchQueue.main.async {
                completed?(error)
            }
            
        }
    }
    
    func pushUnubscribe(completed: ((Error?)->())?) {
        guard let token = ConfigStorage.shared.firebaseToken else {
            completed?(nil)
            return
        }
        let parameters = ["token": token] as [String: AnyObject]
        NetworkService.shared.getJsonData(endpoint: .pushUnsubscribe, parameters: parameters, assync: true) { (json, error) in
            DispatchQueue.main.async {
                completed?(error)
            }
        }
    }
    
}
