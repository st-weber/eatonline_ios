//
//  InfoPage.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import SwiftyJSON

struct InfoPage: ResponseSerializable {

    let name: String
    let endpoint: String
    
    init?(json: JSON) {
        guard let name = json.dictionary?["name"]?.string, let endpoint = json.dictionary?["endpoint"]?.string else { return nil }
        self.name = name
        self.endpoint = endpoint
    }
    
}
