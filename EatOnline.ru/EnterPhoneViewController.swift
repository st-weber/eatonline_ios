//
//  EnterPhoneViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 18.02.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit
import InputMask

class EnterPhoneViewController: UniversalViewController, MaskedTextFieldDelegateListener {

    var maskDelegate = MaskedTextFieldDelegate(primaryFormat: String.phoneMask)
    
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var nextButton: UIButton!
    
    override func viewDidLoad() {
        self.showBasket = false
        super.viewDidLoad()

        maskDelegate.listener = self
        phoneField.delegate = maskDelegate
        phoneField.placeholder = String.phonePlaceholder
        
        checkNext()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        phoneField.becomeFirstResponder()
    }

    func checkNext() {
        guard let phone = phoneField.text else { return }
        if phone.isValidPhoneNumber {
            nextButton.isEnabled = true
            nextButton.backgroundColor = .appRed
            self.view.endEditing(true)
        } else {
            nextButton.isEnabled = false
            nextButton.backgroundColor = .appLightText
        }
    }
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        checkNext()
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        guard let phone = phoneField.text else { return }
        Router.shared.pushController(.checkPhone(phone, true))
    }
    
}
