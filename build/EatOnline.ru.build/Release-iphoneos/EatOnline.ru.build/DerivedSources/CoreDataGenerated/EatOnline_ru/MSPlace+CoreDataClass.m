//
//  MSPlace+CoreDataClass.m
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import "MSPlace+CoreDataClass.h"
#import "MSAction+CoreDataClass.h"

#import "MSCity+CoreDataClass.h"

#import "MSDish+CoreDataClass.h"

#import "MSDishType+CoreDataClass.h"

#import "MSOrder+CoreDataClass.h"

#import "MSTable+CoreDataClass.h"

@implementation MSPlace

@end
