//
//  SimpleCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class SimpleCell: UniversalTableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet var leftPadding: NSLayoutConstraint!
    @IBOutlet var checkedImage: UIImageView!
    @IBOutlet var checkedWidth: NSLayoutConstraint!
    
    override func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        switch content {
        case let content as (String?, Bool):
            leftPadding.constant = 16
            label.text = content.0
            label.alpha = content.1 ? 0.6 : 1
            checkedImage.isHidden = true
            checkedWidth.constant = 0
        case let content as (City, Bool):
            label.text = content.0.name
            label.alpha = content.0.parentCity == nil ? 1 : 0.6
            leftPadding.constant = content.0.parentCity == nil ? 16 : 32
            if content.1 {
                checkedImage.isHidden = false
                checkedWidth.constant = 16
            } else {
                checkedImage.isHidden = true
                checkedWidth.constant = 0
            }
        default:
            break
        }
    }
    
}

extension Cell {
    
    convenience init(simpleTitle: String?, light: Bool = false) {
        self.init(cellType: .simple)
        self.content = (simpleTitle, light)
    }
    
    convenience init(city: City, checked: Bool = false) {
        self.init(cellType: .simple)
        self.content = (city, checked)
    }
    
}
