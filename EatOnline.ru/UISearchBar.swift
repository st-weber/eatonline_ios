//
//  UISearchBar.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 19.04.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

extension UISearchBar {

    func setTextColor(color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
    }
}
