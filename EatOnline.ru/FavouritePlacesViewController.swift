//
//  FavouritePlacesViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class FavouritePlacesViewController: UniversalViewController {

    @IBOutlet var placesTable: TableView!
    
    override func viewDidLoad() {
        self.customTableView = placesTable
        let placesListDS = PlacesListDS(tableInteractor: self, favourites: true)
        self.DS = placesListDS
        super.viewDidLoad()
        updateTable()
        
        placesTable.isHidden = placesListDS.dataSource[0].cells.count == 0
        placesListDS.becomeEmpty = { empty in
            self.placesTable.isHidden = empty
        }
    }

}
