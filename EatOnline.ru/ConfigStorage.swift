//
//  ConfigStorage.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/09/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class ConfigStorage : NSObject {

    @objc static let shared : ConfigStorage = ConfigStorage()
    
    var infoPages: [InfoPage] = []
    
    static var developerMode: Bool {
        if let info = Bundle.main.infoDictionary, let devMode = info["Develop mode"] as? Bool {
            return devMode
        } else {
            return false
        }
    }
    
    var currentCityId: Int? {
        get {
            let currentCityId = UserDefaults.standard.integer(forKey: Keys.currentCityId.rawValue)
            return currentCityId != 0 ? currentCityId : nil
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.currentCityId.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    var currentCityKladr: String? {
        get { return UserDefaults.standard.string(forKey: Keys.currentCityKladr.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.currentCityKladr.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var operatorPhone: String? {
        get { return UserDefaults.standard.string(forKey: Keys.settingsOperatorPhone.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.settingsOperatorPhone.rawValue) }
    }
    @objc var operatorPhoneFormatted: String? {
        get { return UserDefaults.standard.string(forKey: Keys.settingsOperatorPhone.rawValue)?.formatPhone() }
    }
    
    @objc var ownerPhone: String? {
        get { return UserDefaults.standard.string(forKey: Keys.settingsOwnerPhone.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.settingsOwnerPhone.rawValue) }
    }
    @objc var ownerPhoneFormatted: String? {
        get { return UserDefaults.standard.string(forKey: Keys.settingsOwnerPhone.rawValue)?.formatPhone() }
    }
    
    
    @objc var hotlinePhone: String? {
        get { return UserDefaults.standard.string(forKey: Keys.settingsHotlinePhone.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.settingsHotlinePhone.rawValue) }
    }
    @objc var hotlinePhoneFormatted: String? {
        get { return UserDefaults.standard.string(forKey: Keys.settingsHotlinePhone.rawValue)?.formatPhone() }
    }
    
    
    @objc var authHash: String? {
        get { return UserDefaults.standard.string(forKey: Keys.authHash.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.authHash.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var loginPhone: String? {
        get { return UserDefaults.standard.string(forKey: Keys.loginPhone.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.loginPhone.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var myName: String? {
        get { return UserDefaults.standard.string(forKey: Keys.myName.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.myName.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var myAddress: String? {
        get { return UserDefaults.standard.string(forKey: Keys.myAddress.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.myAddress.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var myEntrance: String? {
        get { return UserDefaults.standard.string(forKey: Keys.myEntrance.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.myEntrance.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var myFloor: String? {
        get { return UserDefaults.standard.string(forKey: Keys.myFloor.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.myFloor.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var myIntercom: String? {
        get { return UserDefaults.standard.string(forKey: Keys.myIntercom.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.myIntercom.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var myRoom: String? {
        get { return UserDefaults.standard.string(forKey: Keys.myRoom.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.myRoom.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var firebaseToken: String? {
        get { return UserDefaults.standard.string(forKey: Keys.firebaseToken.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.firebaseToken.rawValue); UserDefaults.standard.synchronize() }
    }
    
    @objc var pushesEnabled: Bool {
        get { return UserDefaults.standard.bool(forKey: Keys.pushesEnabled.rawValue) }
        set { UserDefaults.standard.set(newValue, forKey: Keys.pushesEnabled.rawValue); UserDefaults.standard.synchronize() }
    }
    
}

fileprivate enum Keys: String {
    case currentCityId = "current_city_id"
    case currentCityKladr = "current_city_kladr"
    case settingsOperatorPhone = "settings_operator_phone"
    case settingsOwnerPhone = "settings_owner_phone"
    case settingsHotlinePhone = "settings_hotline_phone"
    
    case authHash = "auth_hash"
    
    case loginPhone = "login_phone"
    
    case myName = "my_name"
    case myAddress = "my_address"
    case myEntrance = "my_entrance"
    case myFloor = "my_floor"
    case myIntercom = "my_intercom"
    case myRoom = "my_room"
    
    case firebaseToken = "firebase_token"
    case pushesEnabled = "pushes_enabled"
}
