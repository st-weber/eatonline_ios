//
//  UniversalTableViewCell.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 30/10/2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class UniversalTableViewCell: UITableViewCell, CellContentProtocol {
    
    func fillWithContent(content: Any?, eventListener: CellEventProtocol?) {
        // Need to implement
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
