//
//  AddressPickerViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 22.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit
import WebKit
import MapKit

class AddressPickerViewController: UniversalViewController {

    let place: Place
    var locationManager = CLLocationManager()
    var currentAddress: String?
    var currentDeliveryPrice: String?
    var currentLocation: CLLocation?
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var suggestionsTable: TableView!
    @IBOutlet var deliveryPriceLabel: UILabel!
    @IBOutlet var continueButton: UIButton!
    
    let apiInteractor = ApiInteractor()
    
    //@IBOutlet var webView: UIWebView!
    @IBOutlet var webView: WKWebView!
    @IBOutlet var webIndicator: UIActivityIndicatorView!
    
    init(place: Place) {
        self.place = place
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.showBasket = false
        self.customTableView = suggestionsTable
        let suggestionsDS = AddressSuggestionsDS(tableInteractor: self, viewController: self)
        self.DS = suggestionsDS
        super.viewDidLoad()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        //webView.delegate = self
        
        setContinueEnabled(false)
        suggestionsTable.isHidden = true
        suggestionsDS.becomeEmpty = { empty in
            print(empty)
            self.suggestionsTable.isHidden = empty
        }
        suggestionsDS.addressSelected = { newAddress in
            self.addressSelected(newAddress)
        }
        if let currentAddress = currentAddress, currentAddress != "не указан" {
            searchBar.text = currentAddress
            Basket.current.currentAddress = currentAddress
            suggestionsTable.isHidden = true
            view.endEditing(true)
            self.detectLocationByAddress(currentAddress)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let request = URLRequest(url: URL(string: "\(URL.api)zones.php?place=\(place.id)&width=\(Int(webView.frame.width))&height=\(Int(webView.frame.height))")!)
        print(request)
        webView.load(request)
        //webView.loadRequest(request)
    }
    
    func detectLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func addressSelected(_ address: AddressSuggestion) {
        searchBar.text = address.value
        Basket.current.currentAddress = address.value
        suggestionsTable.isHidden = true
        view.endEditing(true)
        if let geoLat = address.geoLat, let geoLon = address.geoLon {
            self.showMyMarker(geoLat: geoLat, geoLon: geoLon)
        } else {
            self.detectLocationByAddress(address.value)
        }
    }
    
    func showMyMarker(geoLat: String, geoLon: String) {
        let script = "showMyMarker([\(geoLon),\(geoLat)]);"
        print(script)
        webView.evaluateJavaScript(script, completionHandler: nil)
    }
    
    func detectLocationByAddress(_ address: String) {
        guard let currentCityId = ConfigStorage.shared.currentCityId,
        let cityName = CoreDataManager.instance.getCityById(currentCityId)?.name else { return }
        let script = "detectLocation('\(cityName) \(address)');"
        print(script)
        webView.evaluateJavaScript(script, completionHandler: nil)
    }
    
    func deliveryPriceDetected(_ price: String) {
        if price.count > 0, price != "undefined" {
            currentDeliveryPrice = price
            deliveryPriceLabel.text = "Стоимость доставки: \(price)"
            setContinueEnabled(true)
        } else {
            currentDeliveryPrice = nil
            deliveryPriceLabel.text = "Стоимость доставки: не определена"
            setContinueEnabled(false)
        }
    }
    
    func setContinueEnabled(_ enabled: Bool) {
        continueButton.isEnabled = enabled
        continueButton.backgroundColor = enabled ? .appRed : .appLightText
    }
    
    @IBAction func continuePressed(_ sender: Any) {
        guard let currentDeliveryPrice = currentDeliveryPrice else { return }
        place.setDetectedDeliveryPrice(currentDeliveryPrice)
        dismissController()
    }
    
}

extension AddressPickerViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.last, currentLocation == nil {
            currentLocation = userLocation
            showMyMarker(geoLat: userLocation.coordinate.latitude.description, geoLon: userLocation.coordinate.longitude.description)
            apiInteractor.currentAddress(lat: userLocation.coordinate.latitude.description, lon: userLocation.coordinate.longitude.description) { (suggestions) in
                if let address = suggestions.first?.streetAndHouse {
                    self.searchBar.text = address
                    Basket.current.currentAddress = address
                    NotificationCenter.post(type: .addressDetected)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
}


extension AddressPickerViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webIndicator.stopAnimating()
        if let currentAddress = Basket.current.currentAddress {
            searchBar.text = currentAddress
            detectLocationByAddress(currentAddress)
        } else if currentAddress == nil {
            detectLocation()
        }
    }
    
}

extension AddressPickerViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        deliveryPriceDetected(message)
        completionHandler()
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(true)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        completionHandler("")
    }
}


extension AddressPickerViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        currentAddress = nil
        guard let addressSuggestionsDS = self.DS as? AddressSuggestionsDS else { return }
        self.setContinueEnabled(false)
        addressSuggestionsDS.addressUpdated(newAddress: searchText)
    }
    
}
