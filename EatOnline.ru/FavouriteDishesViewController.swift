//
//  FavouriteDishesViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 28.12.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import UIKit

class FavouriteDishesViewController: UniversalViewController {

    @IBOutlet var dishesTable: TableView!
    
    override func viewDidLoad() {
        self.customTableView = dishesTable
        let dishesDS = FavouriteDishesDS(tableInteractor: self)
        self.DS = dishesDS
        super.viewDidLoad()
        updateTable()
        
        dishesTable.isHidden = dishesDS.dataSource[0].cells.count == 0
        dishesDS.becomeEmpty = { empty in
            self.dishesTable.isHidden = empty
        }
    }


}
