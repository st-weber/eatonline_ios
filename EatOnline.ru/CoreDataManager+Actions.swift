//
//  CoreDataManager+Actions.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 29.11.2019.
//  Copyright © 2019 Michail Solyanic. All rights reserved.
//

import CoreData
import SwiftyJSON

extension CoreDataManager {

    func getActionById(_ id: Int) -> Action? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Action")
        request.predicate = NSPredicate(format: "id = %@", "\(id)")
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let action = result.first as? Action {
                return action
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func getActions(placeId: Int? = nil) -> [Action] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Action")
        if let placeId = placeId {
            request.predicate = NSPredicate(format: "place.id = %@", "\(placeId)")
        }
        
        //let sortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        //request.sortDescriptors = [sortDescriptor]
        do {
            let result = try persistentContainer.viewContext.fetch(request)
            if let actions = result as? [Action] {
                return actions
            } else {
                return []
            }
        } catch {
            return []
        }
    }
    
    func createOrUpdateActions(json: [JSON]) {
        //clearActions()
        
        var oldActions: [Int: Action] = [:]
        for action in getActions() {
            oldActions[Int(action.id)] = action
        }
        
        for actionJson in json {
            if let action = createOrUpdateAction(json: actionJson) {
                oldActions.removeValue(forKey: Int(action.id))
            }
        }
        
        for oldAction in oldActions.values {
            persistentContainer.viewContext.delete(oldAction)
        }
        saveContext()
    }
    
    private func createOrUpdateAction(json: JSON) -> Action? {
        guard let dict = json.dictionary,
            let id = dict["id"]?.int,
            let placeId = dict["place"]?.int else { return nil }
    
        var action: Action?
        if let coreAction = getActionById(id) {
            action = coreAction
        } else {
            action = Action.init(context: persistentContainer.viewContext)
            action?.id = Int32(id)
        }
        
        action?.title = dict["title"]?.string
        action?.type = Int16(dict["type"]?.int ?? 0)
        action?.place = getPlaceById(placeId)
        action?.onMainScreen = dict["title"]?.bool ?? false
        action?.imageWidth = Int16(dict["imageWidth"]?.int ?? 0)
        action?.imageHeight = Int16(dict["imageHeight"]?.int ?? 0)
        action?.imagePath = dict["image"]?.string
        return action
    }

}
