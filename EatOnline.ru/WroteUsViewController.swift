//
//  WroteUsViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 15.01.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class WroteUsViewController: UniversalViewController, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet var contactField: UITextField!
    @IBOutlet var messageView: UITextView!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    let apiInteractor = ApiInteractor()
    
    override func viewDidLoad() {
        self.showBasket = false
        self.keyboardManager.delegate = self
        super.viewDidLoad()

        contactField.layer.borderColor = UIColor(white: 214.0/255.0, alpha: 1.0).cgColor
        contactField.layer.borderWidth = 1.0;
        
        messageView.backgroundColor = .white
        messageView.layer.borderColor = UIColor(white: 214.0/255.0, alpha: 1.0).cgColor
        messageView.layer.borderWidth = 1.0;
        indicator.stopAnimating()
    }

    @IBAction func contactFieldChanged(_ sender: UITextField) {
        checkSendButton()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        checkSendButton()
        return true
    }
    
    func checkSendButton() {
        if let contact = contactField.text, contact.count > 0, let message = messageView.text, message.count > 0 {
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        guard let contact = contactField.text, let message = messageView.text else { return }
        sendButton.isUserInteractionEnabled = false
        sendButton.setTitle(nil, for: .normal)
        indicator.startAnimating()
        apiInteractor.sendWrotaUsLetter(contact: contact, text: message) { (error) in
            self.indicator.stopAnimating()
            if let error = error {
                self.sendButton.setTitle("ОТПРАВИТЬ", for: .normal)
                self.sendButton.isUserInteractionEnabled = true
                Alert.showError(error)
            } else {
                self.sendButton.setTitle("ОТПРАВЛЕНО", for: .normal)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.goBack()
                }
            }
        }
    }
    

}

extension WroteUsViewController: KeyboardEventsDelegate {
    
    func keyboardWillShow(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat) {
        bottomConstraint.constant = keyboardHeight
        UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func keyboardWillHide(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat) {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: duration, delay: 0, options: animationOptions, animations: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.view.layoutIfNeeded()
        }, completion: nil)
    }
}
