//
//  KeyboardManager.swift
//  Direct Farm
//
//  Created by Michail Solyanic on 16/01/2019.
//  Copyright © 2019 Levon Hovsepyan. All rights reserved.
//

import UIKit

protocol HideKeyboardDelegate {
    var hideKeyboardByTouchView: UIView? { get }
}

protocol KeyboardEventsDelegate {
    func keyboardWillShow(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat)
    func keyboardWillHide(duration: TimeInterval, animationOptions: UIView.AnimationOptions, keyboardHeight: CGFloat)
}

class KeyboardManager {

    var delegate: KeyboardEventsDelegate?
    var touchDelegate: HideKeyboardDelegate?
    var keyboardShowed: Bool = false
    lazy var hideKeyboardGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(hideKeyboardByTap))
    }()
    
    func beginMonitoring() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowOrHide(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowOrHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func stopMonitoring() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardDidShow(_ notification: Notification) {
         keyboardShowed = true
    }
    
    @objc func keyboardDidHide(_ notification: Notification) {
        keyboardShowed = false
    }
    
    @objc func keyboardWillShowOrHide(_ notification: Notification) {
        let info = (notification as NSNotification).userInfo ?? [:]
        let duration = TimeInterval((info[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.floatValue ?? 0.25)
        let curve = UInt((info[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue ?? 0)
        let options = UIView.AnimationOptions(rawValue: curve)
        let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? CGRect.zero
        let keyboardHeight = (keyboardRect.height - bottomSafeAreaHeight())
        if notification.name == UIResponder.keyboardWillShowNotification {
            delegate?.keyboardWillShow(duration: duration, animationOptions: options, keyboardHeight: keyboardHeight)
            touchDelegate?.hideKeyboardByTouchView?.addGestureRecognizer(hideKeyboardGesture)
        } else if notification.name == UIResponder.keyboardWillHideNotification {
            delegate?.keyboardWillHide(duration: duration, animationOptions: options, keyboardHeight: keyboardHeight)
            touchDelegate?.hideKeyboardByTouchView?.removeGestureRecognizer(hideKeyboardGesture)
        }
    }
        
    @objc func hideKeyboardByTap() {
        touchDelegate?.hideKeyboardByTouchView?.endEditing(true)
    }
    
    private func bottomSafeAreaHeight() -> CGFloat {
        guard let rootView = UIApplication.shared.keyWindow else { return 0 }
        if #available(iOS 11.0, *) {
            return rootView.safeAreaInsets.bottom
        } else {
            return 0
        }
    }
    
}
