//
//  OrderCreatedViewController.swift
//  EatOnline.ru
//
//  Created by Michail Solyanic on 22.04.2020.
//  Copyright © 2020 Michail Solyanic. All rights reserved.
//

import UIKit

class OrderCreatedViewController: UniversalViewController {

    let orderNumber: Int
    @IBOutlet var numberLabel: UILabel!
    
    init(orderNumber: Int) {
        self.orderNumber = orderNumber
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberLabel.text = "Ваш заказ № \(orderNumber) принят"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    @IBAction func myOrdersPressed(_ sender: UIButton) {
        if let navigationController = navigationController {
            navigationController.viewControllers.removeLast(navigationController.viewControllers.count - 1)
            Router.shared.pushController(.myOrders)
        }
    }
    
}
