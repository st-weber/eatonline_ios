//
//  EatOnline_ru+CoreDataModel.h
//  
//
//  Created by Michail Solyanic on 25.04.17.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "MSAction+CoreDataClass.h"
#import "MSCity+CoreDataClass.h"
#import "MSDish+CoreDataClass.h"
#import "MSDishType+CoreDataClass.h"
#import "MSOrder+CoreDataClass.h"
#import "MSOrderAmount+CoreDataClass.h"
#import "MSPlace+CoreDataClass.h"
#import "MSSettings+CoreDataClass.h"
#import "MSTable+CoreDataClass.h"




